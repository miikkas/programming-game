/**
   A module that defines a NodeTree object, which describes one function in a game level.

   @module nodetree
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {
    
    var p = platformer;
    
    /**
       A NodeTree object, which describes one function in a game level.
       
       @class NodeTree
       @constructor
       @param root {LevelNode} The root node of the tree. If one is not given, it will be created
       automatically.
       @param [name=""] {string} The name of the tree.
       @return {NodeTree} The initialized NodeTree object.
    */
    var NodeTree = function(name, root) {
        this.id_counter = 0;
        this.name = p.default_to(name, 1);
        
        if (typeof root != "undefined") {
            this.root = root;
        } else {
            this.root = new p.LevelNode();
        }
        this.root.set_id(this.id_counter);
        
        this.nodes = {}
        this.nodes[this.root.id.toString()] = this.root;
        this.id_counter++;
        
        return this;
    };
    
    
    
    var ptype = NodeTree.prototype;
    
    /**
       Sets the given node as the root of the tree.
       
       @method set_root
       @param root {LevelNode} The new root node.
    */
    ptype.set_root = function(root) {
        this.root = root;
        this.root.set_id(0);
    };
    
    /**
       Returns the nodes of the tree as an Array.
       
       @method get_nodes
       @return {Array} The array of nodes.
    */
    ptype.get_nodes = function() {
        var id;
        var nodes = [];
        for (id in this.nodes) {
            nodes.push(this.nodes[id]);
        }
        return nodes;
    };
    
    /**
       Returns the number of nodes in the tree.
       
       @method get_node_count
       @return {int} The number of nodes in the tree.
    */
    ptype.get_node_count = function() {
        var count = 0;
        for (var property in this.nodes) {
            if (this.nodes.hasOwnProperty(property)) {
                count++;
            }
        }
        return count;
    };
    
    /**
       Searches and returns a node from the tree by a given ID.
       
       @metod get_node_by_id
       @param id {int} The ID of the node to be searched.
       @return {LevelNode} The searched node or null if a node with the specified ID
       is not found.
    */
    ptype.get_node_by_id = function(id) {
        var node = this.nodes[id.toString()];
        if (typeof node === "undefined") {
            return null;
        }
        return node;
    };
    
    /**
       Searches and returns a node from the tree by a given depth and branch.
       
       @method get_node_by_coords
       @param depth {int} The depth level of the node in the tree.
       @param branch {int} The branch level of the node in the tree.
       @return {LevelNode} The searched node or null if a node with the specified
       depth and branch level is not found.
    */
    ptype.get_node_by_coords = function(depth, branch) {
        var node, id;
        for (id in this.nodes) {
            node = this.nodes[id];
            if (node.depth === depth && node.branch === branch) {
                return node;
            }
        }
        return null;
    };
    
    /**
       Searches and returns a node from the tree by a given element.
       
       @method get_node_by_element
       @param element {Element} The element of the node to be searched.
       @return {LevelNode} The searched node or null if a node with the specified
       element is not found.
    */
    ptype.get_node_by_element = function(element) {
        var node, id
        for (id in this.nodes) {
            node = this.nodes[id];
            if (node.element === element) {
                return node;
            }
        }
        return null;
    };
   
    /**
       Returns the leaf with the greatest depth in the tree.
       
       @method get_last_leaf
       @return {LevelNode} The leaf node.
    */
    ptype.get_last_leaf = function() {
        return this.root.get_last_desc_on_branch();
    };
    
    /**
       Returns the depth of the tree.
       
       @method get_depth
       @return {int} The depth of the tree.
    */
    ptype.get_depth = function() {
        return this.get_last_leaf().depth + 1;
    };
    
    /**
       Returns the branch height of the tree.
       @method get_height
       @return {int} The branch height of the tree.
    */
    ptype.get_height = function() {
        var node, id;
        var height = 0;
        for (id in this.nodes) {
            node = this.nodes[id];
            if (node.branch > height) {
                height = node.branch;
            }
        }
        return height + 1;
    };
    
    /**
       Checks if the tree has a given node.
       
       @method has_node
       @param node {LevelNode} The node to be checked.
       @return {boolean} True if the node is in the tree, otherwise false.
    */
    ptype.has_node = function(node) {
        if (node === null || node.id === null || !this.has_node_with_id(node.id)) {
            return false;
        }
        return true;
    };
    
    /**
       Checks if the tree has a node with a given id.
       
       @method has_node_with_id
       @param id {int} The id of the node to be searched.
       @return {boolean} True if the node is in the tree, otherwise false.
    */
    ptype.has_node_with_id = function(id) {
        if (this.get_node_by_id(id) === null) {
            return false;
        }
        return true;
    };
    
    /**
       Checks if the tree has a node in given coordinates.
       
       @method has_node_with_coords
       @param depth {int} The depth level to be checked.
       @param branch {int} The branch level to be checked.
       @return {boolean} True if the node exists, otherwise false.
    */
    ptype.has_node_with_coords = function(depth, branch) {
        if (this.get_node_by_coords(depth, branch) === null) {
            return false;
        }
        return true;
    };
    
    /**
       Checks if the tree has one or more nodes on a given depth level between
       given branch coordinates including the coordinates themselves.
       
       @method has_node_between_coords
       @param depth {int} The depth coordinate.
       @param branch1 {int} The lesser branch coordinate.
       @param branch2 {int} The greater branch coordinate.
       @return {boolean} True if a node is found, otherwise false.
    */
    ptype.has_node_between_coords = function(depth, branch1, branch2) {
        for (var b = branch1; b < branch2 + 1; b++) {
            if (this.has_node_with_coords(depth, b)) {
                return true;
            }
        }
        return false;
    };
    
    /**
       Finds the potential partners of the given node from the tree. That is, the nodes
       that are on the same depth level, are leafs and have the same branch node as the
       given node.
       
       @method find_potential_partners
       @param node {LevelNode} The node whose potential partners are to be searched.
       @return {Array} An array of potential partner nodes.
    */
    ptype.find_potential_partners = function(node) {
        var partner;
        var partners = [];
        var branch_node = node.branch_node;
        
        if (branch_node.get_child_count() > 2) {
            for (var i = 0; i < branch_node.get_child_count(); i++) {
                partner = branch_node.children[i].get_last_desc_on_branch();
                if (partner != node &&
                    partner.depth === node.depth &&
                    partner.branch != branch_node.branch) {
                    partners.push(partner);
                }
            }
        }
        return partners;
    };
    
    /**
       Recursively modifies the branch level of a node and all its decendants until a node 
       is encountered whose child is on a lower branch level than the node itself or there
       are no more descendants.
       
       @method move_child_branches
       @param node {LevelNode} The node whose branch level is modified and whose descendants 
       are traversed.
       @param branch {int} The branch level to which the branch property of the node should 
       be set.
       @param [highest] {int} The branch level of the highest traversed child.
       @return {int} The branch level of the highest traversed child.
    */
    ptype.move_child_branches = function(node, branch, highest) {
        var child;
        highest = p.default_to(highest, branch);
        
        for (var i = 0; i < node.get_child_count(); i++) {
            child = node.children[i];
            if (child.branch > node.branch) {
                highest = this.move_child_branches(child, 
                                                   child.branch + branch - node.branch,
                                                   highest);
            } else if (child.branch === node.branch) {
                highest = this.move_child_branches(child, branch, highest);
            }
        }
        
        node.branch = branch;
        if (branch > highest) {
            highest = branch;
        }
        return highest;
    };
    
    /**
       Checks if the node and its descendants, which are on the same or higher branch level
       than the node itself, can be moved onto a lower branch level.
       
       @method allows_branch_lowering
       @param node {LevelNode} The node which is checked and whose descendants are traversed.
       @param branch {int} The lower branch level to which the nodes are to be moved.
       @return {boolean} True if lowering is possible, otherwise false.
    */
    ptype.allows_branch_lowering = function(node, branch) {
        if (branch >= node.branch || 
            this.has_node_between_coords(node.depth, branch, node.branch - 1)) {
            return false;
        }
        
        if (node.get_child_count() > 0) {
            var child = node.get_lowest_child();
            if (child.branch < node.branch) {
                return true;
            } else {
                return this.allows_branch_lowering(child, branch);
            }
        } else if (this.has_node_between_coords(node.depth + 1, branch, 
                                                node.branch - 1)) {
            return false;
        }
        return true;
    };
    
    /**
       Recursively traverses the ancestor nodes of a given node and searches for
       branching points. When such point is found, changes the branch level of 
       the nodes in the upper branch. Branches, whose connecting point to its lower
       branch is encountered, are skipped.
       
       @method move_ancestor_branches
       @param node {LevelNode} The node whose ancestors are traversed.
       @param branch {int} The branch level to which the branches should be moved.
       @param [skip=0] {int} Indicates how many subsequent branching points should be
       skipped.
    */
    ptype.move_ancestor_branches = function(node, branch, skip) {
        var highest;
        var parent_count = node.get_parent_count();
        var parent = node.get_highest_parent();
        skip = p.default_to(skip, 0);
        
        if (parent_count > 1) {
            skip++;
            this.move_ancestor_branches(parent, branch, skip);
        } else if (parent_count === 1) {
            for (var i = 0; i < parent.get_child_count(); i++) {
                var sibling = parent.children[i];
                if (sibling != node && sibling.branch > node.branch) {
                    if (skip > 0) {
                        skip--;
                        break;
                    } else if (sibling.branch < branch ||
                               this.allows_branch_lowering(sibling, branch)) {
                        highest = this.move_child_branches(sibling, branch);
                        if (highest >= branch) {
                            branch = highest + 1;
                        }
                    }
                }
            }
            this.move_ancestor_branches(parent, branch, skip);
        }
    };
    
    /**
       Recursively increments the depth of a node and all its decendants until a node 
       is encountered whose child is on a lower branch level than the node itself.
       
       @method stretch_branch
       @param node {LevelNode} The node whose depth is incremented and whose descendants
       are traversed.
    */
    ptype.stretch_branch = function(node) {
        var child_count = node.get_child_count();
        
        node.depth++;
        if (child_count > 0 && node.get_lowest_child().branch < node.branch) {
            return;
        }
        for (var i = 0; i < child_count; i++) {
            this.stretch_branch(node.children[i]);
        }
    };
    
    /**
       Stretches the branch of a given node and also other branches on the same depth level 
       if necessary. The stretch concerns the nodes that are on the greater depth level than 
       the given node. New nodes are created to fill the gaps that the stretching produces.
       
       @method stretch_branches
       @param node {LevelNode} The node that determines which branches are stretched and 
       starting from which depth level.
       @param b_added {Array} The branches to which an inner node has already been added.
    */
    ptype.stretch_branches = function(node, b_added) {
        var n, partner, new_node;
        var child = node.get_highest_child();
        var partners = node.get_partners();
    
        this.stretch_branch(child);    
        for (id in this.nodes) {
            n = this.nodes[id];
            if (n != node && n.depth === node.depth && !b_added.contains(n.branch)) {
                if (n.branch_node === node.branch_node ||
                    n.branch === node.branch_node.branch || 
                    (n.has_lower_descendant() && node.branch === n.branch_node.branch)) {
                    b_added.push(n.branch);
                    this.add_inner(new p.LevelNode(), n.get_lowest_parent(), n, b_added);
                }
            }
        }
        for (var i = 0; i < partners.length; i++) {
            partner = partners[i];                
            new_node = new p.LevelNode();
            new_node.set_tree_coords(partner.depth + 1, partner.branch, 
                                     partner.branch_node);
            new_node.insert_between(partner, child);
            this.map_node_to_id(new_node);
        }
    };
    
    /**
       Maps a node to an ID and adds it to the ID based hashmap of the tree
       
       @method map_node_to_id
       @param node {LevelNode} The node to be mapped to an id.
    */
    ptype.map_node_to_id = function(node) {
        node.set_id(this.id_counter);
        this.nodes[node.id.toString()] = node;
        this.id_counter++;
    };
    
    /**
       Checks if the tree allows the adding of a node that has the coords received as a. 
       parameter. The following conditions must be met:
         - If the node is not of the main branch, there must exist a node that is on
           the same branch as the given branch node and which has the depth received as a 
           parameter.
         - The nodes on the branches below must not have parents or children which are on
           the same branch or higher than the node itself.
         - The node must not have the same depth with an intersection node on the branch 
           level of the branch node if one of the parents of the intersection node has the 
           same branch node as the node itself has.
    
       @method allows_node_with_coords
       @param coords {NodeCoords} The coordinate object, which contains the depth, branch 
       and branch_node, to be edited.
       @return {boolean} True if the adding is allowed, otherwise false.
    */
    ptype.allows_node_with_coords = function(coords) {
        var node;
        var depth = coords.depth;
        var branch = coords.branch; 
        var branch_node = coords.branch_node;
        
        if (branch === 0) {
            return true;
        }
        
        for (var b = 0; b < branch; b++) {
            if (this.has_node_with_coords(depth, b)) {
                node = this.get_node_by_coords(depth, b);
                if (node === this.root || 
                    node.get_highest_parent().branch >= branch ||
                    (node.get_child_count() > 0 && 
                     node.get_highest_child().branch >= branch)) {
                    return false;
                }
            }
        }
        
        if (this.has_node_with_coords(depth, branch_node.branch)) {
            node = this.get_node_by_coords(depth, branch_node.branch);
            if (!node.connects_branches_from(branch_node)) {
                return true;
            }
        }
        return false;
    };
    
    /**
       Checks if the tree allows given nodes to be parents for a new node.
       The following conditions must be met:
         - The nodes must be in the tree.
         - They must not have children.
         - Their depths must be equal.
         - Excluding the lowest one, their branch nodes must be equal.
         - The lowest node must be on the same branch as the branch node of the
           rest of the nodes.
       Also edits given coordinate so that they correspond to the coordinates,
       where the child node should be added.
    
       @method allows_as_parents
       @param nodes {Array} An array of parent nodes to be checked.
       @param coords {NodeCoords} The coordinate object, which contains the depth, branch 
       and branch_node, to be edited.
       @return {boolean} True if the nodes are allowed as parents, otherwise false.
    */
    ptype.allows_as_parents = function(nodes, coords) {
        var node;
        var lowest = p.LevelNode.find_lowest_node(nodes);
        var depth = nodes[0].depth;
        var branch_node = nodes.omit(lowest)[0].branch_node;
        
        if (lowest.branch != branch_node.branch) {
            return false;
        }
        
        for (var i = 0; i < nodes.length; i++) {
            node = nodes[i];
            if (!this.has_node(node) || 
                node.get_child_count() > 0 || 
                node.depth != depth ||
                (node != lowest && node.branch_node != branch_node)) {
                return false;
            }
        }
        
        lowest.clone_def_child_coords(coords);
        return true
    };
    
    /**
       Checks if the tree allows a new child for a node that already has child(ren).
       The following conditions must be met:
         - The node must be in the tree.
         - The node must not have multiple parents.
         - The node must not be the root of the tree.
       Also edits given coordinates so that they correspond to the coordinates,
       where the child node should be added.
       
       @method allows_another_child
       @param parent {LevelNode} The parent node to be checked.
       @param coords {NodeCoords} The coordinate object, which contains the depth, branch 
       and branch_node, to be edited.
       @return {boolean} True if the new child is allowed, otherwise false.
    */
    ptype.allows_another_child = function(parent, coords) {
        if (!this.has_node(parent) || 
            parent.get_parent_count() > 1 || 
            parent === this.root) {
            return false;
        }
        
        var sibling = parent.get_highest_child();
        coords.depth = sibling.depth;
        
        if (sibling.get_child_count() > 1) {
            coords.branch = sibling.get_highest_child().branch + 1;
        } else {
            coords.branch = sibling.branch + 1;
        }
        
        coords.branch_node = parent;
        return true;
    };
    
    /**
       Checks if the tree allows adding a leaf node for given parent(s). Also, if necessary,
       edits given coordinates so that they correspond to the coordinates, where the leaf 
       node should be added.
       
       @method allows_leaf
       @param parents {Array} An array of parent nodes to be checked.
       @param [coords] {NodeCoords} The coordinate object, which contains the depth, branch 
       and branch_node, to be edited.
       @return {boolean} True if the adding of the leaf node is allowed, otherwise false.
    */
    ptype.allows_leaf = function(parents, coords) {        
        var parent = parents[0];
        
        if (typeof coords === "undefined") {
            coords = parent.get_def_child_coords();
        } else {
            parent.clone_def_child_coords(coords);
        }
        
        if (parents.length > 1) {
            if (!this.allows_as_parents(parents, coords)) {
                return false;
            }
        } else if (parent.get_child_count() > 0) {
            if (!this.allows_another_child(parent, coords)) {
                return false;
            }
        } else if (!this.has_node(parent)) {
            return false;
        }
        
        if (!this.allows_node_with_coords(coords)) {
            return false;
        }
        return true;
    };
    
    /**
       Adds a leaf node to the tree as a child of given parents if possible. If the 
       branch of the parent node has sibling branches that which have the same branch node,
       adds leafs to each of those branches, whose last node is on the same depth level as
       the parent node. Also lifts branches of the tree, if necessary, so that the node can 
       be fitted into it.
       
       @method add_leaf
       @param node {LevelNode} The node to be added to the tree.
       @param parents {Array} An array of parent nodes.
       @return {boolean} True if the adding was succesful, otherwise false.
    */
    ptype.add_leaf = function(node, parents) {
        var parent = parents[0];
        var coords = new p.NodeCoords(0, 0, null);
        
        if (!this.allows_leaf(parents, coords)) {
            return false;
        }
        
        if (parent.get_child_count() > 0) {
            this.move_ancestor_branches(parent, coords.branch + 1);
        } 
        node.set_tree_coords(coords.depth, coords.branch, coords.branch_node);
        node.create_child_relations(parents);
        this.map_node_to_id(node);
        
        if (parents.length === 1 && parent.branch === coords.branch) {
            var nodes = this.find_potential_partners(parent);
            if (nodes.length > 0) {
                this.add_leaf(new p.LevelNode(), [nodes[0]]);
            }
        }
        return true;
    };
    
    /**
       Checks if the tree allows adding an inner node between given parent and child. Also, 
       if necessary, edits given coordinates so that they correspond to the coordinates, 
       where the inner node should be added.
       
       @method allows_inner
       @param parent {LevelNode} The parent node to be checked.
       @param child {LevelNode} The child node to be checked.
       @param [coords] {NodeCoords} The coordinate object, which contains the depth, branch 
       and branch_node, to be edited.
       @return {boolean} True if the adding of the inner node is allowed, otherwise false.
    */
    ptype.allows_inner = function(parent, child, coords) {
        if (!this.has_node(parent) || !this.has_node(child)) {
            return false;
        }
        
        if (typeof coords === "undefined") {
            coords = parent.get_def_child_coords();
        } else {
            parent.clone_def_child_coords(coords);
        }
        
        if (parent.depth === child.depth - 1) {
            if (!parent.has_child(child)) {
                return false;
            }
            var higher = p.LevelNode.find_highest_node([parent, child]);
            coords.branch = higher.branch;
            coords.branch_node = higher.branch_node;
        } else {
            return false;
        }
        return true;
    };
    
    /**
       Adds an inner node to the tree between given parent and child node if possible. Also,
       stretches branches of the tree so that the node can be fitted into it and, if 
       the integrity of the tree demands it, adds inner nodes to more than one branch at the 
       same depth level.
       
       @method add_inner
       @param node {LevelNode} The node to be added to the tree.
       @param parent {LevelNode} The parent node of the node to be added.
       @param child {LevelNode} The child node of the node to be added.
       @param [b_added] {Array} The branches to which an inner node has already been added.
       @return {boolean} True if the adding was succesful, otherwise false.
    */
    ptype.add_inner = function(node, parent, child, b_added) {
        var coords = new p.NodeCoords(0, 0, null);
        
        if (!this.allows_inner(parent, child, coords)) {
            return false;
        } else if (typeof b_added === "undefined") {
            b_added = [coords.branch];
        }
        
        node.set_tree_coords(coords.depth, coords.branch, coords.branch_node);
        node.insert_between(parent, child);
        this.stretch_branches(node, b_added);
        this.map_node_to_id(node);
        return true;
    };
    
    /**
       Removes a node from the ID hashmap of the tree and sets its ID to null.
       
       @method remove_id_mapping
       @param node {LevelNode} The node whose ID mapping is removed
    */
    ptype.remove_id_mapping = function(node) {
        delete this.nodes[node.id];
        node.set_id(null);
    };
    
    /**
       Recursively removes all descendants of the node until a lower descendant is 
       encountered or there are no more descendants. Lowers the branches of the 
       tree if there is extra space for it after removal.
       
       @method remove_child_branches
       @param node {LevelNode} The node whose child branches will be removed.
    */
    ptype.remove_child_branches = function(node) {
        while (node.get_child_count() > 0) {
            var child = node.get_highest_child();
            if (child.branch < node.branch) {
                child.remove_parent(node)
                break;
            }
            this.remove_id_mapping(child);
            this.remove_child_branches(child);
            this.move_ancestor_branches(child, child.branch);
            child.clear_relations();
        }
    };
    
    /**
       Recursively removes all the branches originating from the node which are on
       a higher branch level than the node itself until a lower descendant is 
       encountered or there are no more descendants. Lowers the branches of the 
       tree if there is extra space for it after removal.
       
       @method remove_higher_child_branches
       @param node {LevelNode} The node whose higher child branches will be removed.
    */
    ptype.remove_higher_child_branches = function(node) {
        while (node.get_child_count() > 1) {
            var child = node.get_highest_child();
            this.remove_id_mapping(child);
            this.remove_child_branches(child);
            this.move_ancestor_branches(child, child.branch);
            child.clear_relations();
        }
    };

    /**
       Recursively decrements the depth of a node and all its decendants until a node 
       is encountered whose child is on a lower branch level than the node itself.
       
       @method compress_branch
       @param node {LevelNode} The node whose depth is decremented and whose descendants
       are traversed.
    */
    ptype.compress_branch = function(node) {
        var child_count = node.get_child_count();
        
        node.depth--;
        if (child_count > 0 && node.get_lowest_child().branch < node.branch) {
            return;
        }
        for (var i = 0; i < child_count; i++) {
            this.compress_branch(node.children[i]);
        }
    };
    
    /**
       Compresses the branch of a given node and also other branches on the same depth level 
       if necessary. The compression concerns the nodes that are on the greater depth level 
       than the given node. Nodes are removed to make way for the compression.
       
       @method compress_branch
       @param node {LevelNode} The node that determines which branches are compressed and 
       starting from which depth level.
    */
    ptype.compress_branches = function(node, b_removed) {
        var n;
        
        for (id in this.nodes) {
            n = this.nodes[id];
            if (n != node && n.depth === node.depth && !b_removed.contains(n.branch)) {
                if (n.branch_node === node.branch_node ||
                    node.branch === n.branch_node.branch ||
                    (node.has_lower_descendant() && 
                     n.branch === node.branch_node.branch)) {
                    b_removed.push(n.branch);
                    this.remove_node(n, b_removed);
                }
            }
        }
        if (node.get_child_count() > 0) {
            var child = node.get_lowest_child();
            if (child.branch === node.branch) {
                this.compress_branch(child);
            }
        }
    };
    
    /**
       Checks if the removing of a given node is allowed. The node must be in the tree and
       it must not be the root of the tree.
       
       @method allows_removing
       @param node {LevelNode} The node to be checked.
       @return {boolean} True if removal is allowed, otherwise false.
    */
    ptype.allows_removing = function(node) {
        if (!this.has_node(node) || this.root === node) {
            return false;
        }
        return true;
    };
   
    /**
       Removes a node from the tree if possible. Compresses branches to fill the gap left by
       the node. Removes also parallel nodes from other branches if necessary to maintain the
       integrity of the tree.
       
       @method remove_node
       @param node {LevelNode} The node to be removed from the tree.
       @return {boolean} True if the removing is succesful, otherwise false.
    */
    ptype.remove_node = function(node, b_removed) {
        if (!this.allows_removing(node)) {
            return false;
        } else if (typeof b_removed === "undefined") {
            b_removed = [node.branch];
        }
        
        var child;
        this.remove_id_mapping(node);
        this.compress_branches(node, b_removed);

        if (node.get_child_count() > 0) {
            child = node.get_lowest_child();
            child.create_relation(node.get_lowest_parent());
            this.remove_higher_child_branches(node);
            child.clear_relation(node);
        }
        this.move_ancestor_branches(node, node.branch);
        node.clear_relations();
        
        return true;
    };
    
    /**
       Checks if the tree allows connecting two nodes that are already in the tree.
       The following conditions must be met:
         - The parent node must not be on the main branch.
         - The parent must be one depth level behind the child node.
         - The child must be on the parent branch of the parent node.
         - The parent must not have any children and the child not more than one.
         - If the child has more than one parent, their branches must originate from the
           branch node of the parent.
         - If the branch of the parent has a sibling branch that is on a higher level,
           there must not be a node on that branch at the depth level of the child.
         - There must not be any nodes between the branches of the parent and child on
           the depth level of either node.
       
       @method allows_connecting
       @param parent {LevelNode} The parent node to be checked.
       @child child {LevelNode} The child node to be checked.
       @return {boolean} True if connecting is allowed, otherwise false.
    */
    ptype.allows_connecting = function(parent, child) {
        var node;
        var branch_node = parent.branch_node;
        var sibling_branch = branch_node.get_highest_child().branch;
        
        if (!this.has_node(parent) || 
            !this.has_node(child) ||
            parent.branch === 0 ||
            parent.depth != child.depth - 1 || 
            parent.get_child_count() > 0 ||
            child.get_child_count() > 1 ||
            !child.branch === parent.branch_node.branch ||
            (child.get_parent_count() > 1 && 
             !child.connects_branches_from(branch_node)) ||
            (sibling_branch > parent.branch && 
             this.has_node_with_coords(child.depth, sibling_branch))) {
            return false;
        }
        
        for (var b = child.branch + 1; b < parent.branch; b++) {
            if (this.has_node_with_coords(child.depth, b)) {
                return false;
            }
        }
        return true;
    };
    
    /**
       Connects two given nodes in the tree if possible.
       
       @method connect_nodes
       @param parent {LevelNode} The parent node to be connected.
       @param child {LevelNode} The child node to be connected.
       @return {boolean} True if connecting was succesful, otherwise false.
    */
    ptype.connect_nodes = function(parent, child) {
        if (!this.allows_connecting(parent, child)) {
            return false;
        }
        child.create_relation(parent);
        var partners = this.find_potential_partners(parent);
        for (var i = 0; i < partners.length; i++) {
            child.create_relation(partners[i]);
        }
        
        return true;
    };
    
    /**
       Checks if the tree allows disconnecting two connected nodes that are already in the tree.
       The child must be on a lower branch than the parent node.
       
       @method allows_disconnecting
       @param parent {LevelNode} The parent node to be checked.
       @child child {LevelNode} The child node to be checked.
       @return {boolean} True if disconnecting is allowed, otherwise false.
    */
    ptype.allows_disconnecting = function(parent, child) {
        if (this.has_node(parent) && 
            this.has_node(child) && 
            parent.has_child(child) &&
            child.branch < parent.branch) {
            return true;
        }
        return false;   
    };
    
    /**
       Disconnects two given nodes in the tree if possible.
       
       @method disconnect_nodes
       @param parent {LevelNode} The parent node to be connected.
       @param child {LevelNode} The child node to be connected.
       @return {boolean} True if disconnecting was succesful, otherwise false.
    */
    ptype.disconnect_nodes = function(parent, child) {
        if (!this.allows_disconnecting(parent, child)) {
            return false;
        }
        child.clear_relation(parent);
        var partners = this.find_potential_partners(parent);
        for (var i = 0; i < partners.length; i++) {
            child.clear_relation(partners[i]);
        }
        return true;
    };
    
    /**
       Removes the node with given ID from the tree.
       
       @method remove_node_by_id
       @param id {int} the ID by which the node is identified.
    */
    ptype.remove_node_by_id = function(id) {
        var node = this.get_node_by_id(id);
        if (node != null) {
            this.remove_node(node);
        }
    };
    
    platformer.NodeTree = NodeTree;
}());
