﻿/**
   Module that performs some further prototyping for Javascript classes, e.g. Array, and 
   contains all general useful functions and objects. This script must appear in the html file 
   before all other platformer scripts except constants.

   @module utils
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {
    var arr_ptype = Array.prototype;
    var str_ptype = String.prototype;
    //http://www.w3schools.com/jsref/jsref_split.asp
    /**
       Removes an item from the array if such item exists.
       
       @method remove
       @param item {Object} The item to be removed.
    */
    arr_ptype.remove = function(item) {
        var index = this.indexOf(item);
        if (index >= 0) {
            this.splice(index, 1);
        }
    };
    
    /**
       Clones and returns the array.
       
       @method clone
       @return {Array} The cloned array.
    */
    arr_ptype.clone = function() {
        return this.slice(0);
    };

    /**
       Returns a clone of the array with a given item removed from it.
       
       @method omit
       @param item {Object} The item to be removed.
       @return {Array} The cloned array without the item.
    */
    arr_ptype.omit = function(item) {
        var start = this.slice(0, this.indexOf(item));
        var end = this.slice(this.indexOf(item) + 1);
        return start.concat(end);
    };
    
    /**
       Extends the array by the values from the array received as a parameter.
       
       @method extend
       @param array {Array} An array whose values are added to the original array.
    */
    arr_ptype.extend = function(array) {
        for (var i = 0; i < array.length; i++) {
            this.push(array[i]);
        }
    };
    
    /**
       Checks if the array contains a given item.
       
       @method contains
       @param item {Object} The item to be checked.
       @return {boolean} True if the array contains the given item, otherwise false.
    */
    arr_ptype.contains = function(item) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === item) {
                return true;
            }
        }
        return false;
    };

    arr_ptype.get_last = function() {
        return this[this.length - 1];
    }
    
    /**
       Returns a string which consists of the original string repeated a given number of 
       times.
       
       @method repeat
       @param n {int} The factor how many times the string is repeated.
       @return {string} The resulting string from repeating the original. If n is smaller 
       than or equal to zero or NaN, the original string is returned.
    */
    str_ptype.repeat = function(n) {
        if (isNaN(n) || n < 0) {
            //console.log("jeah");
            n = 0;
        }
        //console.log(num);
        return new Array(n + 1).join(this);
    };
    
    /**
       Returns a given argument if the argument is not undefined, otherwise a given default
       value.
       
       @param arg {Object} The argument to be checked.
       @param default_val {Object} The value to return if arg was undefined.
       @return {Object} If not undefined, return arg, otherwise default_val.
    */
    platformer.default_to = function(arg, default_val) {
        return typeof arg != "undefined" ? arg : default_val;
    };
    
}());
