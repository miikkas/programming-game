﻿this.platformer = this.platformer || {};

(function() {  
    var p = platformer;
    
    function parse_level(path, ui) {
        var json;
        $.getJSON(path, json, function(data) {
            var level = parse_json_level(data);
            ui.init_level(level, new p.LevelRunner(data.tests, level));
        });
    };
    
    /**
       Create a level by parsing a json presentation.

       @method parse_json_level
       @param json {Object} An anonymous object in json format containing the level data.
       @return {BuildLevel} The level in internal format.
    */
    function parse_json_level(json) {
        var parse_obj = parse_json_elements(json.elements);
        var elements = parse_obj.elements;
        var for_stmt_count = parse_obj.for_stmt_count;
        
        var level = new p.BuildLevel(parse_json_trees(json.trees, for_stmt_count), 
                                     elements);
        
        return level;
    };

    function parse_json_elements(json) {
        var elements = [];
        var param, json_elem, elem;
        var for_stmt_count = 1;
        
        for (var i = 0; i < json.length; i++) {
            json_elem = json[i];
            param = null;
            if (json_elem.type === "for_stmt") {
                param = {iterator : [], id : for_stmt_count++};
            } else if (typeof json_elem.param != "undefined") {
                param = json_elem.param;
            }
            elements.push(new p.Element(json_elem.type, param));
        }
        
        for (i = 0; i < elements.length; i++) {
            elem = elements[i];
            if (elem.type === "for_stmt") {
                elements.push(new p.Element("for_jump", elem));
            }
        }
        return {elements : elements, for_stmt_count : for_stmt_count};
    };
    
    function parse_json_trees(json, for_stmt_count) {
        var trees = [];
        for (var i = 0; i < json.length; i++) {
            trees.push(parse_json_tree(json[i], i + 1, for_stmt_count));
        }
        return trees;
    };
    
    /**
       Create a tree from object list presentation.

       @method create_tree
    */
    function parse_json_tree(json, name, for_stmt_count) {
        var tree, node, parse_obj, parents;
        var for_stmt_stack = [];

        tree = new p.NodeTree(name);
        parse_json_branch(json, tree, [tree.root], for_stmt_count, for_stmt_stack);
        return tree;
    };
    
    function parse_json_branch(json, tree, parents, for_stmt_count, for_stmt_stack) {
        var branch_obj;
        var new_parents = [];
        
        for (var i = 0; i < json.length; i++) {
            branch_obj = json[i];
            if (branch_obj instanceof Array) {
                new_parents.length = 0;
                for (var j = 0; j < branch_obj.length; j++) {
                    new_parents.push(parse_json_branch(branch_obj[j], tree, parents,
                                                       for_stmt_count, for_stmt_stack));
                }
                parents = new_parents;
                continue;
            } else {
                parse_obj = parse_json_node(branch_obj, for_stmt_stack, for_stmt_count);
                node = parse_obj.node;
                for_stmt_count = parse_obj.for_stmt_count;
            }
            tree.add_leaf(node, parents)
            parents = [node];
        }
        return node;
    };
    
    function parse_json_node(json, for_stmt_stack, for_stmt_count) {
        var param = null;
        
        if (json.type === "for_jump") {
            param = for_stmt_stack.pop();
        } else if (json.type === "for_stmt") {
            param = {iterator : [], id : for_stmt_count++};
        } else if (typeof json.param != "undefined") {
            param = json.param;
        }
        
        var parse_obj = {node : new p.LevelNode(new p.Element(json.type, param)),
                         for_stmt_count : for_stmt_count};
        if (json.type === "for_stmt") {
            for_stmt_stack.push(parse_obj.node.element);
        }
        return parse_obj;
    };
    
    platformer.parse_level = parse_level;
    platformer.parse_json_level = parse_json_level;
    platformer.parse_json_elements = parse_json_elements;
    platformer.parse_json_trees = parse_json_trees;
    platformer.parse_json_tree = parse_json_tree;
    platformer.parse_json_branch = parse_json_branch;
    platformer.parse_json_node = parse_json_node;
}());
