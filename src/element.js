/**
   A module that defines an Element object which describes a game element that can be set on
   a level platform.

   @module element
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {

    var p = platformer;
    
    /**
       An element object that describes one game element that corresponds to a code 
       statement.
       
       @class Element
       @constructor
       @param [type=PASS] {string} The type of the element as a string.
       @param [param=null] {Object} A parameter object for the element.
       @param [colors] {Object} An anonymous object containing the object components and
       their corresponding colors.
       @return The initialized Element object.
    */
    var Element = function(type, param, colors) {
        this.type = platformer.default_to(type, p.PASS);
        this.param = platformer.default_to(param, null);
        if (typeof colors != "undefined") {
            this.colors = colors;
        } else {
            this.colors = this.get_default_colors();
        }
        return this;
    };
    
    var E = Element;
    
    /**
       Executes the operations associated with a pass element: moves the player character to 
       the lowest child node of the current node if the current node has children.
       
       @method exec_pass
       @static
       @param player {Player} The player object.
    */
    E.exec_pass = function(player) {
        var node = player.get_current_node();
        if (node.get_child_count() > 0) {
            player.update_position(node.get_lowest_child().id);
        }
    };
    
    /**
       Executes the operations associated with a for_jump element: moves the player 
       character to the node where a given for_stmt element is located and empties the 
       iterators of the for_stmts between the current node and the node where the given
       for_stmt element is located, except if the given for_stmt has an empty iterator, 
       in which case acts like exec_pass. 
       
       @method exec_for_jump
       @static
       @param player {Player} The player object.
       @param for_stmt {Element} A for stmt element.
    */
    E.exec_for_jump = function(player, for_stmt) {
        var tree = player.get_current_tree();
        var stmt_node = tree.get_node_by_element(for_stmt);
        var iterator = for_stmt.param.iterator;
        
        if (iterator && iterator.length === 0) {
            E.exec_pass(player);
        } else {
            var n;
            var node = player.get_current_node();
            var nodes = tree.get_nodes();
            
            for (var i = 0; i < nodes.length; i++) {
                n = nodes[i];
                if (n.element == p.FOR_STMT && 
                    stmt_node.depth < n.depth && 
                    n.depth < node.depth &&
                    n.element.param.iterator) {
                    n.element.param.length = 0;
                }
            }
            player.update_position(stmt_node.id);
        }
    };
    
    /**
       Executes the operations associated with a for_stmt element: acts like exec_pass,
       except when given an iterator, when the empty iterator is either filled with items of
       the corresponding inventory slot of the player, or an item is popped from inventory
       and assigned to the corresponding inventory slot.
       
       @method exec_for_stmt
       @static
       @param player {Player} The player object.
       @param [iterator] {Array} An iterator array of items as strings.
       @param [colors] {Object} An anonymous object containing the element components (the 
       iterator and the element itself) and the corresponding colors.
    */
    E.exec_for_stmt = function(player, param, colors) {
        var iterator = param.iterator;
        
        if (!iterator) {
            E.exec_pass(player);
            return;
        }
            
        if (iterator.length === 0) {
            iterator.extend(player.get_inventory_slot(colors.iterator));
        }
        if (iterator.length > 0) {
            player.set_inventory_slot(colors.element, [iterator.pop()]);
            E.exec_pass(player);
        } else {
            var node;
            var nodes = player.get_current_tree().get_nodes();
            var for_stmt = player.get_current_node().element;
            for (var i = 0; i < nodes.length; i++) {
                node = nodes[i];
                if (node.element.param == for_stmt) {
                    player.update_position(node.id);
                }
            }
        }
    };
    
    /**
       Executes the operations associated with a push element: acts like exec_pass, but
       additionally pushes a given item to the inventory slot of the player, corresponding 
       to a specified element color.
       
       @method exec_push
       @static
       @param player {Player} The player object.
       @param item {string} The item to be pushed to the inventory as a string.
       @param color {Object} An anonymous color object containing the slot color as a value
       of element property.
    */
    E.exec_push = function(player, item, color) {
        E.exec_pass(player);
        player.push_to_inventory(color.element, item);
    };
    
    /**
       Executes the operations associated with a pop element: acts like exec_pass, but
       additionally pops a given item from the inventory slot of the player, corresponding 
       to a specified color, and pushes it to another slot determined by another color.
       
       @method exec_pop
       @static
       @param player {Player} The player object.
       @param param {Object} A dummy param for the exec method of the Element object. Not 
       used. 
       @param colors {Object} An anonymous object containing the element components (a 
       return value and the element itself) and the corresponding colors.
    */
    E.exec_pop = function(player, param, colors) {
        E.exec_pass(player);
        var item = player.pop_from_inventory(colors.element);
        if (item) {
            player.push_to_inventory(colors.return_val, item);
        }
    };
    
    /**
       Executes the operations associated with a delete_stmt element: acts like exec_pass, 
       but additionally pops a given item from the inventory slot of the player,
       corresponding to a specified color.
       
       @method exec_delete_stmt
       @static
       @param player {Player} The player object.
       @param param {Object} A dummy param for the exec method of the Element object. Not 
       used.
       @param color {Object} An anonymous color object containing the slot color as a value
       of element property.
    */
    E.exec_delete_stmt = function(player, param, color) {
        E.exec_pass(player);
        player.pop_from_inventory(color.element);
    };
    
    /**
       Executes the operations associated with an assign element: acts like exec_pass, but
       additionally sets given items as the contents of the inventory slot of the player,
       corresponding to a specified color.
       
       @method exec_assign
       @static
       @param player {Player} The player object.
       @param items {Array} An array of items as string that is set as the contents of an
       inventory slot replacing the old contents.
       @param color {Object} An anonymous color object containing the slot color as a value
       of element property.
    */
    E.exec_assign = function(player, items, color) {
        E.exec_pass(player);
        player.set_inventory_slot(color.element, items.clone());
    };
    
    /**
    Executes the operations associated with a push_random element: acts like exec_pass,
    but additionally pushes an item to the inventory slot of the player, corresponding to 
    a specified element color. An item is chosen randomly from given items or received
    as the next test input from the current level.
    
    @method exec_push_random
    @static
    @param player {Player} The player object.
    @param items {Array} An array of items as strings from which the pushed item must be 
    chosen.
    @param color {Object} An anonymous color object containing the slot color as a value
    of element property.
    */
    E.exec_push_random = function(player, items, item, color) {
        E.exec_pass(player);
        if (item === null || !items.contains(item)) {
            item = items[Math.floor((Math.random() * 10) % items.length)];
        }
        player.push_to_inventory(color.element, item);
    };
    
    /**
    Executes the operations associated with a func_call element: pushes the current
    inventory and the position of the player to their callstack and creates a new 
    inventory. To the new inventory, only the items from the slot of the player's old 
    inventory, corresponding to a specified color, are cloned. Finally, updates the
    position of the player to the beginning of the tree with given name, if possible.
    
    @method exec_func_call
    @static
    @param player {Player} The player object.
    @param tree_name {string} The name of a tree to which the player is moved.
    @param colors {Object} An anonymous object containing the element components (
    arguments and the element itself) the corresponding colors.
    */
    E.exec_func_call = function(player, tree_name, colors) {
        var inventory = new p.Inventory();
        
        inventory[p.RED] = player.get_inventory_slot(colors.args);
        player.push_callstack();
        player.set_inventory(inventory);
        player.update_position(0, tree_name);
    };
    
    /**
    Executes the operations associated with a return_stmt element: pops the last
    function call from the player's callstack, if possible, and updates the position
    of the player's character according to it. Restores also the old inventory, but
    the slot, corresponding to the color specified by the last func_call element, is
    overwritten by the values from the slot of the current inventory determined by a 
    given color. Empties iterators of all the for_stmts in the current tree before
    returning.
    
    @method exec_return_stmt
    @static
    @param player {Player} The player object.
    @param param {Object} A dummy param for the exec method of the Element object. Not 
    used.
    @param color {Object} An anonymous color object containing the slot color as a value
    of element property.
    */
    E.exec_return_stmt = function(player, param, color) {
        var call = player.pop_callstack();
        if (typeof call === "undefined") {
            return;
        }
        var n, return_color, node;
        var inventory = call.inventory;
        var nodes = player.get_current_tree().get_nodes();
        
        for (var i = 0; i < nodes.length; i++) {
            n = nodes[i];
            if (n.element.type === p.FOR_STMT && n.element.param) {
                n.element.param.length = 0;
            }
        }
        
        player.update_position(call.position.id, call.position.tree);
        node = player.get_current_node();
        return_color = node.element.colors.return_val;
        inventory[return_color] = player.get_inventory_slot(color.element);
        player.update_position(node.get_lowest_child().id);
        player.set_inventory(inventory);
    };
    
    /**
       Executes the operations associated with a conditional element: iterates through
       the upper children of the player's current platform node. If a corresponding
       condition is fulfilled by the player's inventory slot, specified by one of the
       given colors, the player is moved to that node. If none of the conditions is
       fulfilled, the player is moved to the lowest child of the current node.
       
       @method exec_conditional
       @static
       @param player {Player} The player object.
       @param conditions {Array} An array of anonymous condition objects, which contain
       item names as properties and their corresponding amounts as values.
       @param colors {Object} An anonymous object containing the element components 
       (the conditions by number) and corresponding colors.
    */
    E.exec_conditional = function(player, conditions, colors) {
        var children = player.get_current_node().children.clone();
        //conditions = conditions.reverse().clone(); is this wrong?
        //.reverse() reverses in place
        conditions = conditions.clone();
        var condition, child;
        
        i = conditions.length - 1;
        while (children.length > 1) {
            condition = conditions.pop();
            child = platformer.LevelNode.find_highest_node(children);
            children.remove(child);
            
            if (typeof condition != "undefined" && 
                player.item_first_in_slot(condition, colors[i])) {
                player.update_position(child.id);
                return;
            }
            i--;
        }
        player.update_position(children.pop().id)
    };
    
    /**
       Executes the operations associated with a sys_exit element: if the inventory slot of
       the player, specified by a given color, fulfills a given condition, the current test
       of the current level is finished.
       
       @method exec_sys_exit
       @static
       @param player {Player} The player object.
       @param condition {Array} A condition array of items as strings.
       @param color {Object} An anonymous color object containing the slot color as a value
       of element property.
    */
    E.exec_sys_exit = function(player, condition, levelrunner, color) {
        var invcontents = player.get_inventory_slot(color.element);
        var inventoryequal = true;

        //invcontents = condition;

        if (invcontents.length != condition.length) {
            inventoryequal = false;
        } else {
            for (var i = 0; i < invcontents.length; i++) {
                if (invcontents[i] != condition[i]) {
                    inventoryequal = false;
                }
            }
        }
        levelrunner.finish_test(inventoryequal);
    };
    
    /**
       A constant property containing the element types and their corresponding execution
       functions.
       
       @property EXEC_MAP
       @for Element
       @type Object
    */
    E.EXEC_MAP = {pass : E.exec_pass,
                  for_jump : E.exec_for_jump,
                  for_stmt : E.exec_for_stmt,
                  push : E.exec_push,
                  pop : E.exec_pop,
                  delete_stmt : E.exec_delete_stmt,
                  assign : E.exec_assign,
                  func_call : E.exec_func_call,
                  return_stmt : E.exec_return_stmt,
                  conditional : E.exec_conditional};
    
    /**
       Executes the operations associated with the element.
       
       @method exec
       @param player {Player} A player object.
    */
    E.prototype.exec = function(player, levelrunner) {
        if (this.type === "push_random") {
            E.exec_push_random(player, this.param, levelrunner.get_next_test_input(), 
                               this.colors);
        } else if (this.type === "sys_exit") {
            E.exec_sys_exit(player, this.param, levelrunner, this.colors);
        } else {
            E.EXEC_MAP[this.type](player, this.param, this.colors);
        }
    };
    
    /**
       Returns an object containing the default components of the element and the 
       corresponding colors defined by the element type. 
       
       @method get_default_colors
       @return {Object} An anonymous object containing the components and colors.
    */
    E.prototype.get_default_colors = function() {
        if ([p.PASS, p.FOR_JUMP, p.FOR_STMT].contains(this.type) && this.param === null) {
            return null;
        }
        var colors = {}
            
        if (this.type === p.FUNC_CALL) {
            colors[p.ARGS] = p.RED;
        } else if (this.type === p.CONDITIONAL) {
            var color_count = this.param.length;
            if (color_count > 3) {
                color_count = 3;
            }
            for (var i = 0; i < color_count; i++) {
                colors[i] = p.RED;
            }
        } else {
            colors[p.ELEMENT] = p.RED;
        }
            
        if (this.type === p.FOR_STMT && this.param) {
            colors[p.ITERATOR] = p.BLUE;    
        } else if (this.type === p.POP || this.type === p.FUNC_CALL) {
            colors[p.RETURN_VAL] = p.BLUE;
        }
        return colors;
    }
    
    platformer.Element = E;
}());
