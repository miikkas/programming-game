/**
   Module that contains the constants used in the game. This script must appear in the 
   html file before all other platformer scripts.

   @module constants
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {
    
    //Element constants
    platformer.PASS = "pass";
    platformer.FOR_JUMP = "for_jump";
    platformer.FOR_STMT = "for_stmt";
    platformer.PUSH = "push";
    platformer.POP = "pop";
    platformer.DELETE = "delete_stmt";
    platformer.ASSIGN = "assign";
    platformer.PUSH_RANDOM = "push_random";
    platformer.FUNC_CALL = "func_call";
    platformer.RETURN_STMT = "return_stmt";
    platformer.CONDITIONAL = "conditional";
    platformer.SYS_EXIT = "sys_exit";
    
    //Color constants
    platformer.RED = "red";
    platformer.BLUE = "blue";
    platformer.GREEN = "green";
    platformer.YELLOW = "yellow";
    platformer.COLORS = [platformer.RED, 
                         platformer.BLUE, 
                         platformer.GREEN, 
                         platformer.YELLOW];
    
    //Element component constants
    platformer.ELEMENT = "element";
    platformer.ITERATOR = "iterator";
    platformer.ARGS = "args";
    platformer.RETURN_VAL = "return_val";
    
    platformer.ACTIONS = ["add", 
                          "remove", 
                          "connect",
                          "disconnect",
                          "clear",
                          "lift"];
    
    platformer.LEVEL_FRAMES = [
        [0,0,32,32],
        [32,0,32,32],
        [64,0,32,32],
        [96,0,32,96],
        [128,0,32,32],
        [160,0,32,37],
        [192,0,32,32],
        [224,0,32,32],
        [256,0,32,32],
        [288,0,32,32],
        [320,0,32,32],
        [352,0,32,32],
        [384,0,32,32],
        [416,0,96,32],
        [512,0,32,32],
        [544,0,32,32],
        [576,0,32,32],
        [608,0,32,32],
        [640,0,32,32],
        [672,0,32,32],
        [704,0,32,32],
        [736,0,32,32],
        [768,0,5,32],
        [773,0,32,32],
        [805,0,32,32],
        [837,0,32,32],
        [869,0,32,96],
        [901,0,32,32],
        [933,0,32,37],
        [965,0,32,32],
        [997,0,32,32],
        [1029,0,32,32],
        [1061,0,32,32],
        [1093,0,32,32],
        [1125,0,32,32],
        [1157,0,32,32],
        [1189,0,32,32],
        [1221,0,96,32],
        [1317,0,32,37],
        [1349,0,32,96],
        [1381,0,32,32],
        [1413,0,32,32],
        [1445,0,32,32],
        [1477,0,32,32],
        [1509,0,32,32],
        [1541,0,6,6],
        [1547,0,6,6],
        [1553,0,6,6],
        [1559,0,6,6],
        [1565,0,32,32],
        [1597,0,32,32],
        [1629,0,32,32],
        [1661,0,32,32],
        [1693,0,32,32],
        [1725,0,32,32],
        [1757,0,32,32],
        [1789,0,32,96],
        [1821,0,32,32],
        [1853,0,32,37],
        [1885,0,32,32],
        [1917,0,32,32],
        [1949,0,32,32],
        [1981,0,32,32],
        [2013,0,32,32],
        [2045,0,32,32],
        [2077,0,32,32],
        [2109,0,96,32],
        [2205,0,32,32],
        [2237,0,11,12],
        [2248,0,11,12],
        [2259,0,11,12],
        [2270,0,11,12],
        [2281,0,32,10],
        [2313,0,32,32],
        [2345,0,32,32],
        [2377,0,32,32],
        [2409,0,32,32],
        [2441,0,32,32],
        [2473,0,32,96],
        [2505,0,32,32],
        [2537,0,32,37],
        [2569,0,32,32],
        [2601,0,32,32],
        [2633,0,32,32],
        [2665,0,32,32],
        [2697,0,32,32],
        [2729,0,32,32],
        [2761,0,32,32],
        [2793,0,96,32]
    ];

    platformer.LEVEL_ANIMATIONS = {
        "blue_delete": 0,
        "blue_elem_ladder": 1,
        "blue_for_stmt": 2,
        "blue_frame": 3,
        "blue_function": 4,
        "blue_high_ladder": 5,
        "blue_ladder": 6,
        "blue_pop_arrow": 7,
        "blue_pop_target": 8,
        "blue_push": 9,
        "blue_question": 10,
        "blue_return_arrow": 11,
        "blue_return": 12,
        "blue_slot": 13,
        "dude": 14,
        "for_ground": 15,
        "for_jump_deact": 16,
        "for_jump": 17,
        "for_pipe_left_bend": 18,
        "for_pipe_right_bend": 19,
        "for_pipe_straight": 20,
        "func_ground": 21,
        "gap": 22,
        "green_delete": 23,
        "green_elem_ladder": 24,
        "green_for_stmt": 25,
        "green_frame": 26,
        "green_function": 27,
        "green_high_ladder": 28,
        "greenish": 29,
        "green_ladder": 30,
        "green_pop_arrow": 31,
        "green_pop_target": 32,
        "green_push": 33,
        "green_question": 34,
        "green_return_arrow": 35,
        "green_return": 36,
        "green_slot": 37,
        "high_ladder": 38,
        "disabled": 39,
        "ladder": 40,
        "lift_frame_bottom": 41,
        "lift_frame_middle": 42,
        "lift_frame": 43,
        "lift_frame_top": 44,
        "num1": 45,
        "num2": 46,
        "num3": 47,
        "num4": 48,
        "orange": 49,
        "pass": 50,
        "pink": 51,
        "pop_frame": 52,
        "red_delete": 53,
        "red_elem_ladder": 54,
        "red_for_stmt": 55,
        "red_frame": 56,
        "red_function": 57,
        "red_high_ladder": 58,
        "red_ladder": 59,
        "red_pop_arrow": 60,
        "red_pop_target": 61,
        "red_push": 62,
        "red_question": 63,
        "red_return_arrow": 64,
        "red_return": 65,
        "red_slot": 66,
        "root": 67,
        "smallgreenish": 68,
        "smallorange": 69,
        "smallpink": 70,
        "smallviolet": 71,
        "sys_exit_label": 72,
        "sys_exit": 73,
        "violet": 74,
        "yellow_delete": 75,
        "yellow_elem_ladder": 76,
        "yellow_for_stmt": 77,
        "yellow_frame": 78,
        "yellow_function": 79,
        "yellow_high_ladder": 80,
        "yellow_ladder": 81,
        "yellow_pop_arrow": 82,
        "yellow_pop_target": 83,
        "yellow_push": 84,
        "yellow_question": 85,
        "yellow_return_arrow": 86,
        "yellow_return": 87,
        "yellow_slot": 88
    };

    platformer.MENU_FRAMES = [
        [0,0,240,268],
        [240,0,32,48],
        [272,0,32,32],
        [304,0,240,150],
        [544,0,240,350],
        [784,0,32,48],
        [816,0,32,32],
        [848,0,32,48],
        [880,0,32,32],
        [912,0,122,88],
        [1034,0,32,48],
        [1066,0,32,32]
    ];

    platformer.MENU_ANIMATIONS = {
        "actions": 0,
        "blue_mouse": 1,
        "blue": 2,
        "colors": 3,
        "elements": 4,
        "green_mouse": 5,
        "green": 6,
        "red_mouse": 7,
        "red": 8,
        "test": 9,
        "yellow_mouse": 10,
        "yellow": 11
    };

    platformer.MAIN = 1;
}());
