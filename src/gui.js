/**
   A module that defines a UserInterface object, which initializes the
   EaselJS stage, loads the graphics and sets the tick event handler.

   @module gui
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {
    
    var p = platformer;

    /**
       A SpriteData object that contains the data associated with a certain sprite.
                                        
       @class SpriteData
       @constructor
       @param type {string} The type of the sprite corresponding to a .png image.
       @param elem_part {string} The part of an element that the sprite represents.
       @param y {int} The offset of the sprite in y direction from the default position.
       @param x {int} The offset of the sprite in x direction from the default position.
       @return {SpriteData} The initialized SpriteData object.
    */ 
    var SpriteData = function(type, elem_part, y, x) {
        this.type = type;
        this.elem_part = p.default_to(elem_part, null);
        this.y = p.default_to(y, 0);
        this.x = p.default_to(x, 0);
        return this;
    };

    /**
       A SpriteGetters object that contains the getter methods for sprite data associated
       with a node and with an inventory element.

       @class SpriteGetters
       @constructor
       @param get_node_sprite_data {Function} The method returning the sprite data
       associated with a node.
       @param get_inv_sprite_data {Function} The method returning the sprite data
       associated with an inventory element.
       @return {SpriteGetters} The initialized SpriteGetters object.
    */
    var SpriteGetters = function(get_node_sprite_data, get_inv_sprite_data) {
        this.get_node_sprite_data = get_node_sprite_data;
        this.get_inv_sprite_data = platformer.default_to(get_inv_sprite_data, 
                                                          get_node_sprite_data);
        return this;
    };

    /**
       A UserInterface object that handles all GUI related operatios.

       @class UserInterface
       @constructor
       @return {UserInterface} The initialized UserInterface object.
    */
    var UserInterface = function() {
        this.initialized = false;
        return this;
    };
    
    var UI = UserInterface;
    var ptype = UserInterface.prototype;

    UI.BG_HEIGHT = 950;
    UI.BG_EXT_HEIGHT = 234;
    UI.INVENTORY_X = 20;
    UI.TILE_SIDE = 32;
    UI.GAP_SIZE = 5;
    UI.TREES_X = UI.INVENTORY_X * 2 + UI.TILE_SIDE * 3;
    UI.TREES_Y = -17;
    UI.MENU_X = UI.TREES_X + UI.TILE_SIDE * 16 + UI.GAP_SIZE * 15 + 42;
    UI.MENU_Y = 20;
    UI.MENU_WIDTH = 240;
    UI.TEST_Y = 20;
    UI.COLORS_Y = 779;
    UI.ELEM_COLS_X = UI.MENU_X + 2;
    UI.ELEM_ROWS_Y = UI.TILE_SIDE * 2 + 56;
    UI.BRANCH_TILE_COUNT = 5;
    UI.DUDE = "dude";
    UI.BRANCH_HEIGHT = UI.TILE_SIDE * UI.BRANCH_TILE_COUNT + 5;
    UI.LADDER_OFFSET = 5 / UI.TILE_SIDE;
    UI.CONTENT_OFFSET = 0.67;
    
    /**
       Creates an EaselJs stage from a given canvas and sets its mouse event frequency.
       
       @method create_stage
       @static
       @param canvas {canvas} The canvas object acting as a base for the stage.
       @param mouse_freq {int} The frequency of maximum mouse event broadcasts per second. 
       @return {stage} The initalized stage object.
    */
    UI.create_stage = function(canvas, mouse_freq) {
        var stage = new createjs.Stage(canvas);
        stage.width = canvas.width;
        stage.height = canvas.height;
        stage.enableMouseOver(mouse_freq); // Apparently this takes a lot of CPU

        return stage;
    };

    /**
      Loads the graphics of the user inteface as spritesheets from a given directory.
      
      @method load_graphics
      @static
      @param {directory} The path of the directory under which the sprite directories are
      located.
      @return {Object} An object containing a spritesheets for level and menu sprites.
    */
    UI.load_graphics = function(directory) {

        var level_img = new Image();
        level_img.src = directory + "level_sprites/sprites.png";

        var level_sprites = new createjs.Sprite(new createjs.SpriteSheet({
            images : [level_img],
            // The following is automatically generated and copy pasted from
            // the output of the script in graph/sprite_elements directory.
            // Refer to the README.txt there and re-run the script always when
            // you update the spritesheet!
            frames : p.LEVEL_FRAMES,
            animations : p.LEVEL_ANIMATIONS
        }), 0);

        var menu_img = new Image();
        menu_img.src = directory + "menu_sprites/sprites.png";

        var menu_sprites = new createjs.Sprite(new createjs.SpriteSheet({
            images : [menu_img],
            frames : p.MENU_FRAMES,
            animations : p.MENU_ANIMATIONS
        }), 0);

        return {level_sprites : level_sprites, menu_sprites : menu_sprites};
    };
    
    /**
       Returns the width of the sprite of a given type.
       
       @method get_sprite_width
       @static
       @param type {string} The type of the sprite.
       @return {int} The width of the sprite.
    */
    UI.get_sprite_width = function(type) {
        return p.MENU_FRAMES[p.MENU_ANIMATIONS[type]][2];
    };
    
    /**
       Returns the height of the sprite of a given type.
       
       @method get_sprite_height
       @static
       @param type {string} The type of the sprite.
       @return {int} The height of the sprite.
    */
    UI.get_sprite_height = function(type) {
        return p.MENU_FRAMES[p.MENU_ANIMATIONS[type]][3];
    };
    
    /**
      Returns the required sprite data for drawing a ladder of the conditional element.
      
      @method get_ladder_data
      @static
      @param nodes {Array} An array of child nodes of the node in which the element
      resides.
      @param branch {int} The tree branch of the node in which the element resides.
      @param element {Element} The conditional element.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_ladder_data = function(nodes, branch, element) {
        var node, b_diff;
        var color = "";
        var data = [];
        var node_count = -1;
        var calculate_y = function(tile, br) {
            return tile + br * UI.BRANCH_TILE_COUNT + br * UI.LADDER_OFFSET;
        };
        
        while (nodes.length > 0) {
            node = p.LevelNode.find_lowest_node(nodes);
            nodes.remove(node);
            for (var b = branch; b < node.branch; b++) {
                b_diff = b - branch;
                for (var t = 2; t < UI.BRANCH_TILE_COUNT; t++) {
                    data.push(new SpriteData("ladder", null, calculate_y(t, b_diff)));
                }
                if (typeof element != "undefined" && b === node.branch - 1) {
                    color = element.colors[node_count] + "_";
                    data.push(new SpriteData(element.param[node_count], null, 
                                             calculate_y(t, b_diff) + 0.60));
                }
                data.push(new SpriteData(color + "ladder", node_count, 
                                         calculate_y(t, b_diff)));
                data.push(new SpriteData(color + "high_ladder", node_count,
                                         calculate_y(t + 1, b_diff) + 
                                         UI.LADDER_OFFSET));
            }
            //branch = b;
            node_count++;
        }
        return data;
    };
    
    /**
      Returns the required sprite data for the contents of an element frame.
      
      @method get_frame_content_data
      @static
      @param contents {Array} The contents of the frame as strings.
      @param content_y {int} The offset of the contents in y direction.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_frame_content_data = function(contents, content_y) {
        var data = [];
        
        for (var i = 0; i < contents.length; i++) {
            data.push(new SpriteData(contents[i], null,
                                     i * UI.CONTENT_OFFSET + content_y));
        }
        return data;
    };
    
    /**
      Returns the required sprite data for a pass node.
      
      @method get_pass_node_data
      @static
      @return {Array} An array containing a SpriteData object.
    */
    UI.get_pass_node_data = function() {
        return [new SpriteData(p.PASS)];
    };
    
    /**
      Returns the required sprite data for a for jump inventory element.
      
      @method get_for_jump_inv_data
      @static
      @param element {Element} The for jump element.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_for_jump_inv_data = function(element) {
        var for_stmt_param = element.param.param;
        var iterator = for_stmt_param.iterator;
        var data = []
        
        if (iterator && iterator.length > 0 || !iterator) {
            data.push(new SpriteData(p.FOR_JUMP));
        } else {
            data.push(new SpriteData(p.FOR_JUMP + "_deact"));
        }
        data.push(new SpriteData("num" + for_stmt_param.id, null, -0.05, 0.4));
        return data;
    };
    
    /**
      Returns the required sprite data for a node with a for jump element.
      
      @method get_for_jump_node_data
      @static
      @param element {Element} The for jump element.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_for_jump_node_data = function(element) {
        var data = [new SpriteData("for_ground")];
        data.extend(UI.get_for_jump_inv_data(element));
        return data;
    };
    
    /**
      Returns the required sprite data for a for stmt inventory element.
      
      @method get_for_jump_inv_data
      @static
      @param element {Element} The for stmt element.
      @param frame_y {int} The y offset of the inventory frame from the default position.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_for_stmt_inv_data = function(element, frame_y) {
        var colors = element.colors;
        var data = [];
        frame_y = p.default_to(frame_y, 2);
        
        if (element.param) {
            data.push(new SpriteData(colors.iterator + "_frame", p.ITERATOR, 
                                     frame_y));
        }
        data.push(new SpriteData(colors.element + "_for_stmt", p.ELEMENT));
        data.push(new SpriteData("num" + element.param.id, p.ELEMENT, -0.05, 0.4));
        return data;
    };
    
    /**
      Returns the required sprite data for a node with a for stmt element.
      
      @method get_for_stmt_node_data
      @static
      @param element {Element} The for stmt element.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_for_stmt_node_data = function(element) {
        var data = UI.get_for_stmt_inv_data(element, 4);
        data.push(new SpriteData("for_ground", p.ELEMENT));
        return data;
    };
    
    /**
      Returns the required sprite data for a push inventory element.
      
      @method get_push_inv_data
      @static
      @param element {Element} The push element.
      @param elem_y {int} The y offset of the element from the default position.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_push_inv_data = function(element, elem_y) {
        elem_y = p.default_to(elem_y, 0);
        return [new SpriteData(element.colors.element + "_push", p.ELEMENT, elem_y),
                new SpriteData(element.param, null, elem_y)];
    };
    
    /**
      Returns the required sprite data for a node with a push element.
      
      @method get_for_stmt_node_data
      @static
      @param element {Element} The push element.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_push_node_data = function(element) {
        var data = UI.get_pass_node_data();
        data.extend(UI.get_push_inv_data(element, 2));
        return data;
    };
    
    /**
      Returns the required sprite data for a pop inventory element.
      
      @method get_pop_inv_data
      @static
      @param element {Element} The pop element.
      @param elem_y {int} The y offset of the element from the default position.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_pop_inv_data = function(element, elem_y) {
        var colors = element.colors;
        elem_y = p.default_to(elem_y, 0);
        return [new SpriteData(colors.element + "_pop_arrow", p.ELEMENT, elem_y),
                new SpriteData(colors.return_val + "_pop_target", p.RETURN_VAL, elem_y),
                new SpriteData("pop_frame", null, elem_y)];
    };
  
    /**
      Returns the required sprite data for a node with a pop element.
      
      @method get_pop_node_data
      @static
      @param element {Element} The pop element.
      @return {Array} An array of SpriteData objects..
    */  
    UI.get_pop_node_data = function(element) {
        var data = UI.get_pass_node_data();
        data.extend(UI.get_pop_inv_data(element, 2));
        return data;
    };
    
    /**
      Returns the required sprite data for a delete inventory element.
      
      @method get_delete_inv_data
      @static
      @param element {Element} The delete element.
      @param elem_y {int} The y offset of the element from the default position.
      @return {Array} An array containing a SpriteData object.
    */
    UI.get_delete_inv_data = function(element, elem_y) {
        elem_y = p.default_to(elem_y, 0);
        return [new SpriteData(element.colors.element + "_delete", p.ELEMENT, elem_y)];
    };
    
    /**
      Returns the required sprite data for a node with a delete element.
      
      @method get_delete_node_data
      @static
      @param element {Element} The delete element.
      @return {Array} An array of SpriteData objects.
    */  
    UI.get_delete_node_data = function(element) {
        var data = UI.get_pass_node_data();
        data.extend(UI.get_delete_inv_data(element, 1));
        return data;
    };
    
    /**
      Returns the required sprite data for an assign inventory element.
      
      @method get_assign_inv_data
      @static
      @param element {Element} The assign element.
      @param frame_y {int} The y offset of the element frame from the default position.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_assign_inv_data = function(element, frame_y) {
        frame_y = p.default_to(frame_y, 2);
        var data = [new SpriteData(element.colors.element + "_frame", p.ELEMENT, 
                                   frame_y)];
        data.extend(UI.get_frame_content_data(element.param, frame_y - 2));
        return data;
    };
    
    /**
      Returns the required sprite data for a node with an assign element.
      
      @method get_assign_node_data
      @static
      @param element {Element} The assign element.
      @return {Array} An array of SpriteData objects..
    */ 
    UI.get_assign_node_data = function(element) {
        var data = UI.get_pass_node_data();
        data.extend(UI.get_assign_inv_data(element, 4));
        return data;
    };
    
    /**
      Returns the required sprite data for a push random inventory element.
      
      @method get_push_random_inv_data
      @static
      @param element {Element} The push random element.
      @param elem_y {int} The y offset of the element from the default position.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_push_random_inv_data = function(element, elem_y) {
        elem_y = p.default_to(elem_y, 0);
        var param = element.param;
        var elem_colors = element.colors.element;
        
        data = [new SpriteData(elem_colors + "_push", p.ELEMENT, elem_y),
                new SpriteData(elem_colors + "_question", p.ELEMENT, elem_y + 1.1)];
        for (var i = 0; i < param.length; i++) {
            data.push(new SpriteData("small" + param[i], null,
                                     elem_y - 0.5 + (i > 1) * 0.375,
                                     0.15625 + (i % 2) * 0.34375));
        }
        return data;
    };
    
    /**
      Returns the required sprite data for a node with a push random element.
      
      @method get_push_random_node_data
      @static
      @param element {Element} The push random element.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_push_random_node_data = function(element) {
        var data = UI.get_pass_node_data();
        data.extend(UI.get_push_random_inv_data(element, 2));
        return data;
    };
    
    /**
      Returns the required sprite data for a func call inventory element.
      
      @method get_func_call_inv_data
      @static
      @param element {Element} The func call element.
      @param elem_y {int} The y offset of the element's return arrow from the default 
      position.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_func_call_inv_data = function(element, elem_y) {
        var colors = element.colors;
        elem_y = p.default_to(elem_y, 0);

        return [new SpriteData(colors.args + "_function", p.ARGS, elem_y),
                new SpriteData(colors.return_val + "_return_arrow", p.RETURN_VAL, 
                               elem_y + 1),
                new SpriteData("num" + element.param, p.ARGS, elem_y - 0.5, 0.4)];
    };
    
    /**
      Returns the required sprite data for a node with a func call element.
      
      @method get_func_call_node_data
      @static
      @param element {Element} The func call element.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_func_call_node_data = function(element) {
        var data = [new SpriteData("func_ground", p.ARGS)];
        data.extend(UI.get_func_call_inv_data(element, 0));
        return data;
    };
    
    /**
      Returns the required sprite data for a return inventory element.
      
      @method get_return_inv_data
      @static
      @param element {Element} The return element.
      @param elem_y {int} The y offset of the element from the default position.
      @return {Array} An array containing a SpriteData object.
    */
    UI.get_return_inv_data = function(element, elem_y) {
        elem_y = p.default_to(elem_y, 0);
        return [new SpriteData(element.colors.element + "_return", p.ELEMENT, elem_y)];
    };
    
    /**
      Returns the required sprite data for a node with a return element.
      
      @method get_return_node_data
      @static
      @param element {Element} The return element.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_return_node_data = function(element) {
        var data = UI.get_pass_node_data();
        data.extend(UI.get_return_inv_data(element, 1));
        return data;
    };
    
    /**
      Returns the required sprite data for a conditional inventory element.
      
      @method get_conditional_inv_data
      @static
      @param element {Element} The conditional element.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_conditional_inv_data = function(element) {
        var j;
        var data = [new SpriteData("ladder")];
        var colors = element.colors;
        
        for (var i = 0; i < element.param.length; i++) {
            j = i + 1;
            data.push(new SpriteData(colors[i] + "_elem_ladder", j, j));
            data.push(new SpriteData(element.param[i], null, j));
        }
        return data;
    };
    
    /**
      Returns the required sprite data for a node with a conditional element.
      
      @method get_conditional_node_data
      @static
      @param element {Element} The conditional element.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_conditional_node_data = function(element, node) {
        var data = UI.get_pass_node_data();
        data.push(new SpriteData("ladder", null, 1));
        data.extend(UI.get_ladder_data(node.children.clone(), node.branch, element));
        return data;
    };
    
    /**
      Returns the required sprite data for a sys exit inventory element.
      
      @method get_sys_exit_inv_data
      @static
      @param element {Element} The sys exit element.
      @return {Array} An array of SpriteData objects.
    */
    UI.get_sys_exit_inv_data = function(element) {
        var data = [new SpriteData(element.colors.element + "_frame", p.ELEMENT, 2)];
        data.extend(UI.get_frame_content_data(element.param, 0));
        data.push(new SpriteData("sys_exit_label", p.ELEMENT, 0.65625));
        return data;
    };
    
    /**
      Returns the required sprite data for a node with a sys exit element.
      
      @method get_sys_exit_node_data
      @static
      @param element {Element} The sys exit element.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_sys_exit_node_data = function(element) {
        var data = [new SpriteData(p.SYS_EXIT),
                    new SpriteData(element.colors.element + "_frame", p.ELEMENT, 4)];
        data.extend(UI.get_frame_content_data(element.param, 2));
        return data;
    };
    
    /**
      Returns the required sprite data for a connection node.
      
      @method get_connection_node_data
      @static
      @param node {LevelNode} The connection node.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_connection_node_data = function(node) {
        var data = UI.get_pass_node_data();
        data.push(new SpriteData("ladder", null, 1));
        data.extend(UI.get_ladder_data(node.parents.clone(), node.branch));
        return data;
    };
    
    /**
      Returns the required sprite data for a root node.
      
      @method get_root_node_data
      @static
      @param tree_name {string} The name of the tree starting with the root node.
      @return {Array} An array of SpriteData objects..
    */
    UI.get_root_node_data = function(tree_name) {
        var data = UI.get_pass_node_data();
        data.push(new SpriteData("root", null, 1, -5 / 32));
        data.push(new SpriteData("num" + tree_name, null, 0.6, -5 / 32 + 0.2));
        return data;
    };
    
    /**
       A constant property containing the getter methods for sprite data correspondig to
       a node or invetory object with a certain element type.

       @property SPRITE_GETTERS
       @for UserInterface
       @type Object
    */
    UI.SPRITE_GETTERS = {pass : new SpriteGetters(UI.get_pass_node_data),
                        for_jump : new SpriteGetters(UI.get_for_jump_node_data,
                                                     UI.get_for_jump_inv_data),
                        for_stmt : new SpriteGetters(UI.get_for_stmt_node_data,
                                                     UI.get_for_stmt_inv_data),
                        push : new SpriteGetters(UI.get_push_node_data,
                                                 UI.get_push_inv_data),
                        pop : new SpriteGetters(UI.get_pop_node_data,
                                                UI.get_pop_inv_data),
                        delete_stmt : new SpriteGetters(UI.get_delete_node_data,
                                                        UI.get_delete_inv_data),
                        assign : new SpriteGetters(UI.get_assign_node_data,
                                                   UI.get_assign_inv_data),
                        push_random : new SpriteGetters(UI.get_push_random_node_data,
                                                        UI.get_push_random_inv_data),
                        func_call : new SpriteGetters(UI.get_func_call_node_data,
                                                      UI.get_func_call_inv_data),
                        return_stmt : new SpriteGetters(UI.get_return_node_data,
                                                        UI.get_return_inv_data),
                        conditional : new SpriteGetters(UI.get_conditional_node_data,
                                                        UI.get_conditional_inv_data),
                        sys_exit : new SpriteGetters(UI.get_sys_exit_node_data,
                                                     UI.get_sys_exit_inv_data)};
    
    /**
       Returns the sprite data for a given node in a tree with a specified name.

       @method get_node_sprite_data
       @static
       @param node {LevelNode} The node whose data is to be returned.
       @param tree_name {string} The name of the tree in which the node resides.
       @return {Array} An array of SpriteData objects.
    */
    UI.get_node_sprite_data = function(node, tree_name) {
       
        if (node.get_parent_count() > 1) {
            return UI.get_connection_node_data(node);
        } else if (node.get_parent_count() === 0) {
            return UI.get_root_node_data(tree_name);
        }
        var element = node.element;
        return UI.SPRITE_GETTERS[element.type].get_node_sprite_data(element, node);
    };

    /**
       Returns the sprite data for a given element in the element inventory.

       @method get_inv_sprite_data
       @static
       @param element {Element} The element whose data is to be returned.
       @return {Array} An array of SpriteData objects.
    */
    UI.get_inv_sprite_data = function(element) {
        return UI.SPRITE_GETTERS[element.type].get_inv_sprite_data(element);
    };

    /**
       Initializes the EaselJS stage to the given canvas element, sets the given level as 
       the current one, initializes some settings including the ticker event listener, 
       loads the GUI pictures and other graphics etc.
       
       @method initialize
       @param canvas {canvas} The canvas that works as a base for the stage.
    */
    ptype.initialize = function(canvas) {
        this.init_basic_methods();
        this.stage = UI.create_stage(canvas, 10);
        this.slot_sprites = {};
        this.for_slot_sprites = {};  
        this.root_positions = {};
        this.actions = [];
        this.inventory_sprites = [];
        this.level_sprites = [];
        this.menu_sprites = [];
        this.selected_platform = {tree_name : null, node_id : null};
        this.active_color = p.RED;
        this.scroll_timer = null;

        var dir = "../graph/";
        this.graphics = UI.load_graphics(dir);
        this.stage.addChild(new createjs.Bitmap(dir + "miscellanous/bg.png"));
        this.hilight = new createjs.Bitmap(dir + "miscellanous/hilight.png");
        this.bg_ext = new createjs.Bitmap(dir + "miscellanous/bg_ext.png");
        this.create_actions();
        this.draw_menu();
        
        createjs.DisplayObject.suppressCrossDomainErrors = true;
        this.init_ticker(60);
        this.initialized = true;
    };
    
    /**
       Initializes the level and the player obejcts. Also, draws the level.
    */
    ptype.init_level = function(level, levelrunner) {
        while (!this.initialized) {}
        
        this.level = level;
        if (typeof this.levelrunner != "undefined") {
            this.stage.removeChild(this.levelrunner.status_text);
        }
        this.levelrunner = levelrunner;
        this.player = new platformer.Player(level);
        this.init_level_dependent_methods();
        this.clear_level();
        this.update_level();
        this.stage.addChild(this.levelrunner.status_text);
    };
    
    /**
       Initializes all basic methods for the UserInterface object.
       
       @method init_basic_methods
    */
    ptype.init_basic_methods = function() {
    
        /**
           Initializes a ticker to tick with a certain FPS frequency.
           
           @method init_ticker
           @param fps {int} The FPS frequency.
        */
        ptype.init_ticker = function(fps) {
            var ui = this;
            createjs.Ticker.setFPS(fps);
            createjs.Ticker.addEventListener("tick", function() {
                ui.handle_tick();
            });
        };
    
        /**
           Updates the visibility and the position of the platform highlight sprite. 

           @method update_hilight
           @param visible {boolean} Determines if the sprite is set visible or invisible
           (that is, removed from the stage).
           @param [x] {int} Sets the position of the sprite in x direction.
           @param [y] {int} Sets the position of the sprite in y direction.
        */    
        ptype.update_hilight = function(visible, x, y) {
            this.stage.removeChild(this.hilight);
            if (visible) {
                this.stage.addChild(this.hilight);
                this.hilight.x = p.default_to(x, this.hilight.x);
                this.hilight.y = p.default_to(y, this.hilight.y);
            }
        };

        /**
           Sets the platform selected by the player.
           
           @method set_selected_platform
           @param tree_name {string} The name of the tree in which the platform node resides.
           @param node_id {int} The id of the platform node.
        */
        ptype.set_selected_platform = function(tree_name, node_id) {
            this.selected_platform.node_id = node_id;
            this.selected_platform.tree_name = tree_name;
        };

        /**
           Adds a sprite from a given a sprite sheet on the stage to a specified position.

           @method add_sprite
           @param type {string} The type of the sprite correspondig to a .png image.
           @param sheet {SpriteSheet} The sprite sheet from which to read the sprite.
           @param x {int} The position of the sprite on the stage in x direction.
           @param y {int} The position of the sprite on the stage in y direction.
           @return {SpriteSheet} A clone of the original SpriteSheet object, whose active 
           frame is now the sprite. 
        */    
        ptype.add_sprite = function(type, sheet, x, y) {
            sheet = sheet.clone();
            sheet.x = x;
            sheet.y = y;
            sheet.gotoAndPlay(type);
            this.stage.addChild(sheet);
            this.stage.update();
            return sheet;
        };

        /**
           Adds a level sprite on the stage and to the array of level sprites.

           @method add_level_sprite
           @param type {string} The type of the sprite correspondig to a .png image.
           @param x {int} The position of the sprite on the stage in x direction.
           @param y {int} The position of the sprite on the stage in y direction.
           @return {SpriteSheet} A clone of the level sprite sheet, whose active 
           frame is now the sprite. 
        */    
        ptype.add_level_sprite = function(type, x, y) {
            var sprite = this.add_sprite(type, this.graphics.level_sprites, x, y);
            this.level_sprites.push(sprite);
            return sprite;
        };

        /**
           Adds a menu sprite on the stage and to the array of menu sprites.

           @method add_menu_sprite
           @param type {string} The type of the sprite correspondig to a .png image.
           @param x {int} The position of the sprite on the stage in x direction.
           @param y {int} The position of the sprite on the stage in y direction.
           @return {SpriteSheet} A clone of the menu sprite sheet, whose active 
           frame is now the sprite.
        */
        ptype.add_menu_sprite = function(type, x, y) {
            var sprite = this.add_sprite(type, this.graphics.menu_sprites, x, y);
            this.menu_sprites.push(sprite);
            return sprite;
        };

        /**
           Adds an inventory sprite on the stage and to the array of inventory sprites.

           @method add_inventory_sprite
           @param type {string} The type of the sprite correspondig to a .png image.
           @param x {int} The position of the sprite on the stage in x direction.
           @param y {int} The position of the sprite on the stage in y direction.
           @return {SpriteSheet} A clone of the level sprite sheet, whose active 
           frame is now the sprite.
        */ 
        ptype.add_inventory_sprite = function(type, x, y) {
            var sprite = this.add_sprite(type, this.graphics.level_sprites, x, y);
            this.inventory_sprites.push(sprite);
            return sprite;
        };

        /**
           Returns an object containing the coordinates of the platform node sprite on the 
           stage. 

           @method get_node_sprite_coords
           @param node {LevelNode} The platform node.
           @param tree_name {string} The name of the tree in which the node resides.
           @param {Object} An object containing the x,y coordinates of the sprite.
        */    
        ptype.get_node_sprite_coords = function(node, tree_name) {
            return {x : UI.TREES_X + node.depth * (UI.TILE_SIDE + UI.GAP_SIZE) + UI.GAP_SIZE,
                    y : this.root_positions[tree_name] - node.branch * UI.BRANCH_HEIGHT};
        };

        /**
           Adds a sprite, which is associated with a level node, on the stage and sets its
           event listeners. 

           @method add_node_sprite
           @param type {string} The type of the sprite correspondig to a .png image.
           @param x_offset {int} The offset of the sprite on the stage in x direction from
           the position of the node's platform sprite.
           @param y_offset {int} The offset of the sprite on the stage in y direction from
           the position of the node's platform sprite.
           @param node {LevelNode} The node with which the sprite is associated.
           @param tree_name {string} The name of the tree in which the node resides.
           @param [elem_part=null] {string} The part of the element which the sprite represents 
           or null if it isn't part of an element.
        */    
        ptype.add_node_sprite = function(type, x_offset, y_offset, node, tree_name,
                                         elem_part) {
            var coords = this.get_node_sprite_coords(node, tree_name);                           
            var sprite = this.add_level_sprite(type, coords.x + x_offset, 
                                               coords.y + y_offset);
            var ui = this;
            elem_part = p.default_to(elem_part, null);
            
            sprite.on("click", function(event) {
                if (!ui.levelrunner.is_test_running()) {
                    if (event.nativeEvent.button === 0) {
                        ui.select_platform(tree_name, node.id, coords.x, coords.y);
                    } else {
                        ui.change_color(elem_part, node.element.colors, tree_name, node.id);
                    }
                }
            });
            sprite.on("mouseover", function(event) {
                ui.on_mouse_over(this);
            });
            sprite.name = "" + tree_name + " " + node.id;
        };

        /**
           Adds all sprites that are associated with a given node on the stage.
          
           @method add_node_sprites
           @param node {LevelNode} The node whose sprites are added.
           @param tree_name {string} The name of the tree in which the node resides.
        */    
        ptype.add_node_sprites = function(node, tree_name) {
            var sprite_data, x_offset, y_offset;
            var node_data = UI.get_node_sprite_data(node, tree_name);
            
            for (var i = 0; i < node_data.length; i++) {
                sprite_data = node_data[i];
                x_offset = sprite_data.x * UI.TILE_SIDE;
                y_offset = sprite_data.y * UI.TILE_SIDE;
                this.add_node_sprite(sprite_data.type, x_offset, -y_offset, node, tree_name,
                                     sprite_data.elem_part);
            }
            this.add_node_sprite("gap", -UI.GAP_SIZE, 0, node, tree_name);
            if (node.get_child_count() > 0 && node.get_lowest_child().branch < node.branch) {
                this.add_node_sprite("gap", UI.TILE_SIDE, 0, node, tree_name);
            }
        };

        ptype.remove_node_sprites = function(node_id, tree_name) {
            do {
                var sprite = this.stage.getChildByName("" + tree_name + " " + node_id);
                console.log("" + tree_name + node_id);
                this.level_sprites.remove(sprite);
                this.stage.removeChild(sprite);
            } while (sprite != null);            
        }

        ptype.update_node_sprites = function(tree_name, node_id) {
            this.remove_node_sprites(node_id, tree_name);
            this.add_node_sprites(this.level.get_node(tree_name, node_id), tree_name);
        }

        /**
           Draws the slots for a node tree that the level contains.

           @method draw_slots
           @param tree_name {string} The name of the tree.
        */    
        ptype.draw_slots = function(tree_name) {
            for (var i = 0; i < p.COLORS.length; i++) {
                var gap = UI.TILE_SIDE + 5;
                var sprite = this.add_level_sprite(p.COLORS[i] + "_slot", UI.INVENTORY_X, 
                                                   this.root_positions[tree_name] - gap * i);
                sprite.name = tree_name + " " + "slot";
            }
        };

        /**
           Draws a node tree that the level contains.
           
           @method draw_tree
           @param tree {NodeTree} The tree to be drawn.
        */    
        ptype.draw_tree = function(tree, node_id) {
            var node;
            var nodes = tree.get_nodes();
            node_id = p.default_to(node_id, 0);
            var depth = tree.get_node_by_id(node_id).depth;
            
            for (var i = 0; i < nodes.length; i++) {
                node = nodes[i];
                if (node.depth >= depth) {
                    this.add_node_sprites(node, tree.name);
                }
            }
        };
        
        /**
           Adds a sprite for that disables and inventory element.
           
           @method add_disabled_sprite
           @param x {int} The position of the sprite on the stage in x direction.
           @param y {int} The position of the sprite on the stage in y direction.
        */
        ptype.add_disabled_sprite = function(x, y) {
           var disabled = this.add_inventory_sprite("disabled", x, y);
           disabled.cursor = "default";
           this.inventory_sprites.push(disabled);
        };
        
        /**
           Adds the sprites associated with an inventory element on the stage and sets their
           listeners.

           @method add_inventory_elem_sprites
           @param element {Element} The element whose sprites are to be added.
           @param enabled {boolean} True if element interaction is enabled, false otherwise.
           @param x {int} The position to which the sprites will be added in x direction.
           @param y {int} The position to which the sprites will be added in y direction.
        */    
        ptype.add_inventory_elem_sprites = function(element, enabled, x, y) {
            var sprite_data;
            var ui = this;
            var elem_data = UI.get_inv_sprite_data(element);
            
            for (var j = 0; j < elem_data.length; j++) {
                sprite_data = elem_data[j];
                var sprite = this.add_inventory_sprite(sprite_data.type, 
                                                       x + sprite_data.x * UI.TILE_SIDE,
                                                       y - sprite_data.y * UI.TILE_SIDE);
                sprite.on("click", function(event) {
                    if (ui.levelrunner && !ui.levelrunner.is_test_running()) {
                        ui.place_element(element);
                    }
                });
                sprite.on("mouseover", function(event) {
                    ui.on_mouse_over(this);
                });                     
            }
            if (!enabled) {
                this.add_disabled_sprite(x, y - UI.TILE_SIDE * 2);
            }
        };

        /**
           Clears the inventory sprites from the stage and the inventory sprite array.

           @method clear_inventory
        */    
        ptype.clear_inventory = function() {
            while (this.inventory_sprites.length > 0) {
                this.stage.removeChild(this.inventory_sprites.pop());
            }
        };

        /**
           Removes one level sprite from the stage and level_sprites array.

           @method remove_level_sprite
           @param sprite {SpriteSheet} A SpriteSheet object whose current frame is the
           sprite to be removed.
        */
        ptype.remove_level_sprite = function(sprite) {
            this.stage.removeChild(sprite);
            this.level_sprites.remove(sprite);
        };
        
        /**
           Resizes the stage with an extended background image if necessary. Removes the
           unneccessary background images.

           @method resize_stage
        */
        ptype.resize_stage = function() {
            var canvas = this.stage.canvas;
            var last_pos = 0;
            var rp, pos;
            
            for (rp in this.root_positions) {
                pos = this.root_positions[rp] + UI.TILE_SIDE;
                if (pos > canvas.height) {
                    var bm = this.bg_ext.clone();
                    this.stage.addChild(bm);
                    bm.name = "bg_ext" + canvas.height
                    bm.y = canvas.height;
                    canvas.height += UI.BG_EXT_HEIGHT;
                    return;
                } else if (pos > last_pos) {
                    last_pos = pos;
                }
            }
            while (last_pos < canvas.height - UI.BG_EXT_HEIGHT && 
                canvas.height > UI.BG_HEIGHT) {
                this.stage.removeChild(this.stage.getChildByName("bg_ext" + canvas.height));
                canvas.height -= UI.BG_EXT_HEIGHT;
            }
        };

        /**
           Clears the tree from the stage and from the corresponding sprite array.

           @method clear_tree
           @param tree_name {string} The name of the tree.
        */    
        ptype.clear_tree = function(tree_name) {
            var sprite;
            var clone_sprites = this.level_sprites.clone();
 
            for (var i = 0; i < clone_sprites.length; i++) {
                sprite = clone_sprites[i];
                if (sprite.name === null || sprite.name.split(" ")[0] === tree_name.toString()) {
                    this.level_sprites.remove(sprite);
                    this.stage.removeChild(sprite);
                }
            }
        };

        ptype.clear_level = function() {
            while (this.level_sprites.length > 0) {
                var sprite = this.level_sprites.pop();
                if (sprite == this.player_gfx) {
                    this.player_gfx = null;
                }
                this.stage.removeChild(sprite);
            }
        }
        
        /**
           Draws a test button and sets its listeners.
           
           @method draw_test_button
           @param y_offset {int} The position in y direction from which to start drawing on
           the stage.
        */
        ptype.draw_test_button = function(y_offset) {
            var name = "test";
            var sprite_w = UI.get_sprite_width(name);
            var ui = this;
            
            sprite = this.add_menu_sprite(name, UI.MENU_X + (UI.MENU_WIDTH - sprite_w) / 2,
                                          UI.MENU_Y + y_offset);
            sprite.on("click", function(event) {
                if (ui.levelrunner && ui.levelrunner.is_test_ready()) {
                    ui.levelrunner.start_test(ui);
                }
            });
            sprite.on("mouseover", function(event) {
                if (typeof ui.levelrunner === "undefined") {
                    return;
                };
                
                if (ui.levelrunner.is_test_ready() && sprite.cursor != "pointer") {
                    sprite.cursor = "pointer";
                } else if (!ui.levelrunner.is_test_ready() && 
                           sprite.cursor != "default") {
                    sprite.cursor = "default";
                }
            });
            sprite.name = name;
        };
        
        /**
           Draws the menu sprites that are designed as containers for objects.

           @method draw_menu_containers
           @param y_offset {int} The position in y direction from which to start drawing on
           the stage.
        */    
        ptype.draw_menu_containers = function(y_offset) {
            var sprite_name, sprite_frame, sprite_w, sprite_h, sprite;
            var sprite_names = ["actions", "elements", "colors"];
            var draw_y = UI.MENU_Y * 2 + UI.get_sprite_height("test");
            
            for (var i = 0; i < sprite_names.length; i++) {
                sprite_name = sprite_names[i];
                sprite_w = UI.get_sprite_width(sprite_name);
                sprite_h = UI.get_sprite_height(sprite_name);
                sprite = this.add_menu_sprite(sprite_name, 
                                              UI.MENU_X + (UI.MENU_WIDTH - sprite_w) / 2,
                                              draw_y + y_offset);
                sprite.name = sprite_name;
                draw_y += sprite_h + 20;
            }
        };

        /**
           Draws the color menu contents, which are the mouse and color sprites, and sets
           the listeners for color sprites.

           @method draw_color_menu_contents
        */    
        ptype.draw_color_menu_contents = function() {
            var mouse, button, color;
            var ui = this;
            var mouse_name = "mouse";
            var colors_menu_y = this.stage.getChildByName("colors").y
            var add_mouse_sprite = function() {
                return ui.add_menu_sprite(ui.active_color + "_mouse", UI.MENU_X + 104,
                                          colors_menu_y + 40);
            };
            
            mouse = add_mouse_sprite();
            mouse.name = mouse_name;
            for (var i = 0; i < p.COLORS.length; i++) {
                color = p.COLORS[i];
                button = this.add_menu_sprite(color, UI.MENU_X + 42 * i + 41, 
                                              colors_menu_y + 95);
                button.name = color;
                button.on("click", function(event) {
                    ui.stage.removeChild(ui.stage.getChildByName(mouse_name));
                    ui.active_color = this.name;
                    add_mouse_sprite();
                });
                button.cursor = "pointer";
            }
        };

        /**
           Draws the menu, which contains the test button, actions, element inventory and
           the color selector.

           @method draw_menu
           @param y_offset {int} The position in y direction from which to start drawing on
           the stage.
        */    
        ptype.draw_menu = function(y_offset) {
            y_offset = p.default_to(y_offset, 0);
            
            this.draw_test_button(y_offset);
            this.draw_menu_containers(y_offset);
            this.draw_color_menu_contents();
        };

        /**
           Clears the menu sprites from the stage and the menu sprite array.

           @method clear_menu
        */
        ptype.clear_menu = function() {
            while (this.menu_sprites.length > 0) {
                this.stage.removeChild(this.menu_sprites.pop());
            }
        };
        
        /**
           Creates the action text objects and adds them to the actions array.
           
           @method create_actions
        */
        ptype.create_actions = function() {
            var text;
            var area;
            var ui = this;
            
            for (var i = 0; i < p.ACTIONS.length; i++) {
                text = new createjs.Text(p.ACTIONS[i], "bold 24px Ebrima", "#dbe3de");
                area = new createjs.Shape();
                area.graphics.beginFill("#121f22").rect(0, 5,
                                                        text.getMeasuredWidth(), 
                                                        text.getMeasuredLineHeight() - 5);
                text.hitArea = area;    
                text.cursor = "pointer";
                text.on("click", function(event) {
                    if (ui.levelrunner && !ui.levelrunner.is_test_running()) {
                        ui.execute_action(this.text);
                    }
                });
                text.on("mouseover", function(event) {
                    ui.on_mouse_over(this);
                    if (ui.levelrunner && !ui.levelrunner.is_test_running()) {
                        this.color = "#ff3333";
                    }
                });
                text.on("mouseout", function(event) {
                    this.color = "#dbe3de";
                });
                this.actions.push(text);
            }
        };
        
        /**
           Handles the onMouseOver event of a sprite by changing the cursor to pointer if
           no test is running.

           @method on_mouse_over
           @param sprite {SpriteSheet} The SpriteSheet object, whose current frame represents
           the sprite, that launched the event.
        */    
        ptype.on_mouse_over = function(sprite) {
            if (typeof this.levelrunner === "undefined") {
                return;
            };
            
            if (!this.levelrunner.is_test_running() && sprite.cursor != "pointer") {
                sprite.cursor = "pointer";
            } else if (this.levelrunner.is_test_running() && 
                       sprite.cursor != "default") {
                sprite.cursor = "default";
            }
        };

        /**
           A call-back for the tick event handler to update the stage.
           
           @method handle_tick
        */
        ptype.handle_tick = function() {
            if (this.levelrunner && this.levelrunner.is_test_running()) {
                // Test is running - move the player character.
                // When character is ready to move (is directly above a tile), tell player object to update position 
                // or do what ever is available for the current tile.
                // Move the player from a tile to tile, +
                // Handle special, such as for jumps and ifs 
                this.levelrunner.player_move(this.player, this);
            }
            this.stage.update();
        };
    };
    
    /**
       Initializes all methods that depend on the level object of the UserInterface to be
       initialized.
       
       @method init_level_dependent_methods
    */
    ptype.init_level_dependent_methods = function() {
    
        /**
           Updates the graphical y positions of the root nodes of the node trees in the 
           level.
           
           @method update_root_positions
        */
        ptype.update_root_positions = function() {
            var tree;
            var trees = this.level.trees;
            var pos = UI.TREES_Y;
            this.root_positions = {};
            for (var i = 0; i < trees.length; i++) {
                tree = trees[i];
                pos += tree.get_height() * UI.BRANCH_HEIGHT + 10;
                this.root_positions[tree.name] = pos;
            }
        };

        /**
           Updates the action text objects in the action menu according to the platform 
           selected by the user.

           @method update_actions
           @param tree_name {string} The name of the tree in which the platform node resides.
           @param node_id {int} The id of the platform node.
        */    
        ptype.update_actions = function(tree_name, node_id) {
            var action;
            var action_count = 0;
            var actions = this.level.get_allowed_actions(tree_name, node_id);
            var action_menu_y = this.stage.getChildByName("actions").y;
            
            for (var i = 0; i < this.actions.length; i++) {
                action = this.actions[i];
                this.stage.removeChild(action);
                if (actions.contains(action.text)) {
                    action.x = UI.MENU_X + (240 - action.getMeasuredWidth()) / 2;
                    action.y = action_menu_y + action_count * 40 + 65;
                    this.stage.addChild(action);
                    action_count++;
                }
            }
        };
        
        /**
           Performs the needed operations when the user selects one of the platforms in the
           level, which includes updating the selected platform, platform highlight sprite 
           and the action text objects in the action menu.

           @method select_platform
           @param [tree_name=null] {string} The name of the tree in which the platform node 
           resides.
           @param node_id {int} The id of the platform node.
           @param [x] {int} The position of the platform on the stage in x direction.
           @param [y] {int} The position of the platform on the stage in y direction.
        */    
        ptype.select_platform = function(tree_name, node_id, x, y) {
            tree_name = p.default_to(tree_name, null);
            node_id = p.default_to(node_id, null);
        
            this.set_selected_platform(tree_name, node_id);
            if (node_id != null) {
                this.update_hilight(true, x - 2, y - 2);
            } else {
                this.update_hilight(false);
            }
            this.update_actions(tree_name, node_id);
            this.clear_inventory();
            this.draw_inventory_contents(tree_name, node_id);
            this.stage.update();
        };

        ptype.update_platform_selection = function(tree_name, node_id) {
            var node = this.level.get_node(tree_name, node_id);
            if (node != null) {
                var coords = this.get_node_sprite_coords(node, tree_name);
                this.select_platform(tree_name, node_id, coords.x, coords.y);
            } else {
                this.select_platform();
            }
        }
        
        /**
           Updates the menu and its position so that it is always visible.
           
           @method update_menu
           @param pos {int} The position to draw the menu.
        */
        ptype.update_menu = function(pos) {
            var current_tree = this.selected_platform.tree_name;
            var current_node = this.selected_platform.node_id;
        
            this.clear_menu();
            this.draw_menu(pos);
            this.update_inventory(current_tree, current_node);
            this.update_actions(current_tree, current_node);
        };
        
        /**
           Handles the scroll event by updating the menu position with a delay.

           @method on_scroll
           @param event {Event} The scroll event.
        */    
        ptype.on_scroll = function(event) {
            var target = event.srcElement || event.target;
            var ui = this;
            
            if (this.scroll_timer !== null) {
                clearTimeout(this.scroll_timer);
            }
            this.scroll_timer = setTimeout(function() {
                ui.update_menu(target.scrollTop);
            }, 100);
        };

        /**
           Executes the selected action and updates the level and the element inventory 
           accordingly.

           @method execute_action
           @param action {string} The action to execute.
        */    
        ptype.execute_action = function(action) {
            var tree_name = this.selected_platform.tree_name;
            var node_id = this.selected_platform.node_id;
            var elements = this.level.elements;                    
            var type = this.level.get_node(tree_name, node_id).element.type;
            var child_id;
            if (action === "disconnect") {
                child_id = this.level.get_node(tree_name, node_id).get_lowest_child().id;
            }

            this.level.execute_action(tree_name, node_id, action);             
            if (action === "connect") {
                child_id = this.level.get_node(tree_name, node_id).get_lowest_child().id;
            }
            if (action === "clear" && type != p.CONDITIONAL && type != p.FOR_STMT) {
                this.update_node_sprites(tree_name, node_id);
                this.add_to_inventory(elements.get_last(), elements.length - 1, true);                
            } else if (action === "lift") {
                this.update_level(tree_name, node_id);
            } else if (action === "connect" || action === "disconnect") {
                this.update_node_sprites(tree_name, node_id);
                this.update_node_sprites(tree_name, child_id);
            } else {
                this.update_level(tree_name, node_id);
                this.update_inventory(tree_name, node_id);
            }                            
            this.update_platform_selection(tree_name, node_id);
        };

        /**
           Places a given element to the currently selected platform and updates the level
           and the element inventory accordingly.

           @method place_element
           @param element {Element} The element to place on the platform.
        */    
        ptype.place_element = function(element) {
            var tree_name = this.selected_platform.tree_name;
            var node_id = this.selected_platform.node_id;
            var old_type = this.level.get_node(tree_name, node_id).element.type;
            
            this.level.place_element(tree_name, node_id, element);
            var new_type = this.level.get_node(tree_name, node_id).element.type;

            if (old_type != p.CONDITIONAL && old_type != p.FOR_STMT && new_type != p.CONDITIONAL) {
                this.update_node_sprites(tree_name, node_id);
            } else {
                this.update_level(tree_name, node_id);
            }
            this.update_inventory(tree_name, node_id);
            this.update_platform_selection(tree_name, node_id);
        };

        /**
           Changes the color of the sprite if possible and updates the level accordingly.

           @method change_color
           @param elem_part {string} The part of the element which the sprite represents or
           null.
           @param colors {Object} An object containing the element color data.
        */    
        ptype.change_color = function(elem_part, colors, tree_name, node_id) {
            if (colors) {
                colors[elem_part] = this.active_color;
            }
            this.update_node_sprites(tree_name, node_id);
            this.update_platform_selection(tree_name, node_id)
        };
        
        /**
           Draws the level and resizes the stage if needed for the level to fit in.
           Updates also the inventory contents and the selected platform according to
           given tree name and node id.

           @method update_level
           @param tree_name {string} The name of the tree in which the currently selected
           platform node resides.
           @param node_id {int} The currently selected platform node.
           @param mode {string} The mode which determines, which parts of the level shall be redrawn.
        */    
        ptype.update_level = function(tree_name, node_id, mode) {
            var tree, sprites;
            var trees = this.level.trees;
            var positions = this.root_positions;
            tree_name = p.default_to(tree_name, null);
            node_id = p.default_to(node_id, null);
            
            this.update_root_positions();
            this.resize_stage();
            for (var i = 0; i < trees.length; i++) {
                tree = trees[i];
                if (tree.name === tree_name || 
                    positions[tree.name] != this.root_positions[tree.name]) {
                    this.clear_tree(tree.name);
                }
                if (tree_name === null || tree.name === tree_name || 
                    positions[tree.name] != this.root_positions[tree.name]) {
                    this.draw_slots(tree.name);
                    this.draw_tree(tree);
                }
            }
            this.update_platform_selection(tree_name, node_id);
        };

        ptype.update_inventory = function(tree_name, node_id) {
            this.clear_inventory();
            this.draw_inventory_contents(tree_name, node_id);
        }
        
        /**
           Draws the contents of the element inventory that are associated with the selected
           platform.

           @method draw_inventory_contents
           @param tree_name {string} The name of the tree in which the selected platform node
           resides.
           @param node_id {int} The id of the select platform node.
        */
        ptype.draw_inventory_contents = function(tree_name, node_id) {
            var x, y, element;
            var elements = this.level.elements;
            var disabled = this.level.get_disabled_elements(tree_name, node_id);
            
            for (var i = 0; i < elements.length; i++) {
                element = elements[i];
                this.add_to_inventory(element, i, !disabled.contains(element));
            }
        };

        ptype.add_to_inventory = function(element, pos, enabled) {
            var elements_menu_y = this.stage.getChildByName("elements").y
            var ELEM_WIDTH = UI.TILE_SIDE + 2;
            var ELEM_HEIGHT = UI.TILE_SIDE * 3 + 2;

            x = UI.ELEM_COLS_X + (pos % 7) * ELEM_WIDTH;
            y = elements_menu_y + UI.ELEM_ROWS_Y + Math.floor(pos / 7) * ELEM_HEIGHT;
            this.add_inventory_elem_sprites(element, enabled, x, y);
        }
        
        /**
           Initializes the player and resets the inventory and for slots
           prior to running any tests.

           @method init_test
        */
        ptype.init_test = function() {
            // Reset stuff before starting anew
            delete this.player;
            this.player = new platformer.Player(this.level);
            
            // Empty the inventory slots
            for (var key in this.slot_sprites) {
                for (var i = 0; i < this.slot_sprites[key].length; i++) {
                    this.remove_level_sprite(this.slot_sprites[key][i]);
                }
            }
            //delete this.player_gfx;
            this.remove_level_sprite(this.player_gfx);
            
            
            if (typeof this.player_gfx == "undefined" || this.level_sprites.indexOf(this.player_gfx) == -1) {
                var coords = this.get_node_sprite_coords(this.player.position.level.trees[0].get_node_by_id(0), p.MAIN);
                
                delete this.player_gfx;
                this.player_gfx = this.add_level_sprite(UI.DUDE, coords.x, coords.y - UI.TILE_SIDE);
            }
            
            delete this.slot_sprites;
            
            this.slot_sprites = {};

            // Empty the for slots
            for (var key in this.for_slot_sprites) {
                for (var i = 0; i < this.for_slot_sprites[key].length; i++) {
                    this.remove_level_sprite(this.for_slot_sprites[key][i]);
                }
            }
            delete this.for_slot_sprites;
            this.for_slot_sprites = {};
        };
    };
    
    platformer.UserInterface = UserInterface;
}());
