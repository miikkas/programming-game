/**
   A module that defines a LevelNode object, which describes one executable command in a 
   game level. Also contains NodeCoords object which describes a set of LevelNode 
   coordinates in a NodeTree and is utilized by both of these objects.
   
   @module levelnode
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {
    
    var p = platformer;
    
    /**
       A NodeCoords object, which describes a set of node coordinates in a node tree: depth,
       branch and branch_node.
       
       @class NodeCoords
       @constructor
       @param depth {int} The depth coordinate of a node.
       @param branch {int} The branch coordinate of a node.
       @param branch_node {LevelNode} The branch node coordinate of a node.
       @return An initialized NodeCoords object.
    */
    var NodeCoords = function(depth, branch, branch_node) {
        this.depth = depth;
        this.branch = branch;
        this.branch_node = branch_node;
        
        return this;
    };
    
    /**
       Clones the properties of the object itself to a give NodeCoords object.
       
       @method clone_properties
       @param coords {NodeCoords} A NodeCoords object to which the properties are to be 
       cloned.
    */
    NodeCoords.prototype.clone_properties = function(coords) {
        coords.depth = this.depth;
        coords.branch = this.branch;
        coords.branch_node = this.branch_node;
    };
    
    /**
       A LevelNode object, which describes one executable command in a game level.
       
       @class LevelNode
       @constructor
       @param [element=new Element(PASS)] {Element} The element the node contains. 
       @param [parents=[]] {Array} An array containing the parents of the node.
       @param [children=[]] {Array} An array containing the children of the node. Defaults to an
       empty array.
       @return {LevelNode} The initialized LevelNode object.
    */
    var LevelNode = function(element, parents, children) {
        this.id = null;
        this.element = p.default_to(element, new p.Element(p.PASS));
        this.parents = p.default_to(parents, []);
        this.children = p.default_to(children, []);
        this.depth = 0;
        this.branch = 0;
        this.branch_node = this;
        
        return this;
    };
    
    var ptype = LevelNode.prototype;
    
    /**
       Returns the node with either highest or lowest branch level from a given array of nodes or null 
       if the array is empty. If there are several nodes with equal highest or lowest branch, the first 
       one of them is returned.
       
       @method find_node_by_branch
       @static
       @param nodes {Array} An array of nodes.
       @param criterion {int} 1 if node with the highest branch is searched, -1 if the one 
       with the lowest.
       @return {LevelNode} The node with the highest or lowest branch level.
    */
    LevelNode.find_node_by_branch = function(nodes, criterion) {
        if (nodes.length > 0) {
            var node = nodes[0];
            for (var i = 0; i < nodes.length; i++) {
                if (criterion * nodes[i].branch > criterion * node.branch) {
                    node = nodes[i];
                }
            }
            return node;
        } else {
            return null;
        }
    };
    
    /**
       Returns the node with the highest branch level from a given array of nodes or null 
       if the array is empty. If there are several nodes with equal highest branch, the first 
       one of them is returned.
       
       @method find_highest_node
       @static
       @param nodes {Array} An array of nodes.
       @return {LevelNode} The node with the highest branch level.
    */
    LevelNode.find_highest_node = function(nodes) {
        return LevelNode.find_node_by_branch(nodes, 1);
    };
    
    /**
       Returns the node with the lowest branch level from a given array of nodes or null 
       if the array is empty. If there are several nodes with equal lowest branch, the first 
       one of them is returned.
       
       @method find_lowest_node
       @static
       @param nodes {Array} An array of nodes.
       @return {LevelNode} The node with the lowest branch level.
    */
    LevelNode.find_lowest_node = function(nodes) {
        return LevelNode.find_node_by_branch(nodes, -1);
    };
    
    /**
       Checks if all the given nodes have an equal depth. If the optional depth parameter is passed,
       the depths of the nodes are compared to it.
       
       @method compare_depths
       @static
       @param nodes {Array} An array of nodes, whose depths are compared.
       @param [depth] {int} The depth to which the depths of the nodes are compared.
       @return {boolean} True if the depths are equal to each other and to the optional depth parameter
       if it was passed. Otherwise false.
    */
    LevelNode.compare_depths = function(nodes, depth) {
        if (nodes.length === 0) {
            return true;
        }
        var depth = p.default_to(depth, nodes[0].depth);
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].depth != depth) {
                return false;
            }
        }
        return true;
    };
    
    /**
       Checks if all the given nodes have an equal node from which their branch begins. If the optional 
       branch_node parameter is passed, the branch nodes of the given nodes are compared to it.
       
       @method compare_branch_nodes
       @static
       @param nodes {Array} An array of nodes, whose branch nodes are compared.
       @param [branch_node] {int} The branch node to which the branch nodes of the given nodes are compared.
       @return {boolean} True if the branch nodes are equal to each other, and to the optional branch_node
       parameter if it was passed. Otherwise false.
    */
    LevelNode.compare_branch_nodes = function(nodes, branch_node) {
        if (nodes.length === 0) {
            return true;
        }
        var branch_node = p.default_to(branch_node, nodes[0].branch_node);
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].branch_node != branch_node) {
                return false;
            }
        }
        return true;
    };
    
    /**
       Sets the ID of the node to a given value.
       
       @method set_id
       @param id {int} The new ID value.
    */
    ptype.set_id = function(id) {
        this.id = id;
    };
    
    /**
       Sets the parents of the node to be the nodes contained in a given array.
       
       @method set_parents
       @param parents {Array} The new parents of the node.
    */
    ptype.set_parents = function(parents) {
        this.parents = parents;
    };
    
    /**
       Sets the children of the node to be the nodes contained in a given array.
       
       @method set_children
       @param children {Array} The new children of the node.
    */
    ptype.set_children = function(children) {
        this.children = children;
    };
    
    /**
       Adds a new parent for the node.
       
       @method add_parent
       @param parent {LevelNode} The new parent of the node.
    */
    ptype.add_parent = function(parent) {
        this.parents.push(parent);
    };
    
    /**
       Adds a new child for the node.
       
       @method add_child
       @param child {LevelNode} The new child of the node.
    */
    ptype.add_child = function(child) {
        this.children.push(child);
    };
    
    /**
       Removes a parents from the node's parents.
       
       @method remove_parent
       @param {LevelNode} A parent node to be removed.
    */
    ptype.remove_parent = function(parent) {
        this.parents.remove(parent);
    };
    
    /**
       Removes a child from the node's children.
       
       @method remove_child
       @param {LevelNode} A child node to be removed.
    */
    ptype.remove_child = function(child) {
        this.children.remove(child);
    };
    
    /**
       Creates a child-parent relation between given parent node and the node itself
       if the relation does not already exist.
       
       @method create_relation
       @param parent {LevelNode} The parent node.
    */
    ptype.create_relation = function(parent) {
        if (!parent.has_child(this)) {
            parent.add_child(this);
            this.add_parent(parent);
        }
    };
    
    /**
       Create child-parent relations between given parent nodes and the node itself
       if the relations do not already exist.
       
       @method create_child_relations
       @param parents {Array} The array of parent nodes.
    */
    ptype.create_child_relations = function(parents) {
        for (var i = 0; i < parents.length; i++) {
            this.create_relation(parents[i]);
        }
    };
    
    /**
       Create child-parent relations between given child nodes and the node itself
       if the relations do not already exist.
       
       @method clear_parent_relations
       @param children {Array} The array of child nodes.
    */
    ptype.create_parent_relations = function(children) {
        for (var i = 0; i < children.length; i++) {
            children[i].create_relation(this);
        }
    };
    
    /**
       Clears a child-parent relation between given parent node and the node itself if 
       such relation exists.
       
       @method clear_relation
       @param parent {LevelNode} The parent node.
    */
    ptype.clear_relation = function(parent) {
        this.remove_parent(parent);
        parent.remove_child(this);
    };
    
    /**
       Clears the child-parent relations between the node's parents and the node itself.
       
       @method clear_parent_relations
    */
    ptype.clear_parent_relations = function() {
        while (this.get_parent_count() > 0) {
            this.clear_relation(this.parents[0]);
        }
    };
    
    /**
       Clears the child-parent relations between the node's children and the node itself.
       
       @method clear_child_relations
    */
    ptype.clear_child_relations = function() {
        while (this.get_child_count() > 0) {
            this.children[0].clear_relation(this);
        }
    };
    
    /**
       Clears the child-parent relations between the node's children and the node itself, as
       well as between the node's parents and the node itself.
       
       @method clear_relations
    */
    ptype.clear_relations = function() {
        this.clear_parent_relations();
        this.clear_child_relations();
    };
    
    /**
       Inserts a node between given parent and child node by creating relations between them.
       Clears the child-parent relation between the parent and child if such relation exists.
       
       @method insert_between
       @param parent {LevelNode} The parent node.
       @param child {LevelNode} The child node.
    */
    ptype.insert_between = function(parent, child) {
        this.create_relation(parent);
        child.create_relation(this);
        child.clear_relation(parent);
    };
    
    /**
       Removes all the nodes relatives, that is, parents and children.
       
       @method clear_relatives
    */
    ptype.clear_relatives = function() {
        this.parents.length = 0;
        this.children.length = 0;
    };
    
    /**
       Returns the count of node's children.
       
       @method get_child_count
    */
    ptype.get_child_count = function() {
        return this.children.length;
    };
    
    /**
       Returns the count of node's parents.
       
       @method get_parent_count
    */
    ptype.get_parent_count = function() {
        return this.parents.length;
    };
    
    /**
       Returns the parent of the node with the highest branch level or null if the
       node has no parents.
       
       @method get_highest_parent
       @return {LevelNode} The parent of the node with the highest branch level.
    */
    ptype.get_highest_parent = function() {
        return LevelNode.find_highest_node(this.parents);
    };
    
    /**
       Returns the parent of the node with the lowest branch level or null if the
       node has no parents.
       
       @method get_lowest_parent
       @return {LevelNode} The parent of the node with the lowest branch level.
    */
    ptype.get_lowest_parent = function() {
        return LevelNode.find_lowest_node(this.parents);
    };
    
    /**
       Returns the child of the node with the highest branch level or null if the
       node has no children.
       
       @method get_highest_child
       @return {LevelNode} The child of the node with the highest branch level.
    */
    ptype.get_highest_child = function() {
        return LevelNode.find_highest_node(this.children);
    };
    
    /**
       Returns the child of the node with the lowest branch level or null if the
       node has no children.
       
       @method get_lowest_child
       @return {LevelNode} The child of the node with the lowest branch level.
    */
    ptype.get_lowest_child = function() {
        return LevelNode.find_lowest_node(this.children);
    };
    
    /**
       Sets the coordinates of the node in a node tree, which are the node's depth in
       the tree, its branch and the node that is the beginning of the the branch.
       
       @method set_tree_coords
       @param depth {int} The depth level coordinate of the node.
       @param branch {int} The branch level coordinate of the node.
       @param branch_node {LevelNode} The branch node coordinate of the node.
    */
    ptype.set_tree_coords = function(depth, branch, branch_node) {
        this.depth = depth;
        this.branch = branch;
        this.branch_node = branch_node;
    };
    
    /**
       Returns the default coordinates for a child node of the node.
       
       @method get_def_child_coords
       @return {NodeCoords} A coordinate object containing the default child coordinates.
    */
    ptype.get_def_child_coords = function() {
        return new p.NodeCoords(this.depth + 1, this.branch, this.branch_node);
    };
    
    /**
       Clones the default coordinates of the node's child node to a given coordinate object.
       
       @method clone_def_child_coords
       @param coords {NodeCoords} A coordinate object to which the default child coordinates 
       are cloned.
    */
    ptype.clone_def_child_coords = function(coords) {
        this.get_def_child_coords().clone_properties(coords);
    };
    
    /**
       Checks if the node has a given child.
       
       @method has_child
       @param child {LevelNode} The child to be searched.
       @return {boolean} True if the node has the child, otherwise false.
    */
    ptype.has_child = function(child) {
        return this.children.contains(child);
    };
    
    /**
       Returns the partners of the node. That is, the other parents of the node's lowest
       child node if the node has children.
       
       @method get_partners
       @return {Array} An array of partner nodes or an empty array if no partners exist.
    */
    ptype.get_partners = function() {
        if (this.get_child_count() === 0) {
            return [];
        }
        return this.get_lowest_child().parents.omit(this);
    };
    
    /**
       Checks if the node is a connecting point of the branches starting from a given branch
       node.
       
       @method connects_branches_from
       @param node {LevelNode} The branch node from which the branches originate.
       @return {boolean} True if the node is a connecting point, otherwise false.
    */
    ptype.connects_branches_from = function(node) {
        var parent;
        var parent_count = this.get_parent_count();
        var lowest = this.get_lowest_parent();
        
        if (parent_count < 2) {
            return false;
        }
        
        for (var i = 0; i < parent_count; i++) {
            parent = this.parents[i];
            if (parent != lowest && parent.branch_node != node) {
                return false;
            }
        }
        return true;
    };
    
    /**
       Returns the last descendant of the node on its branch.
       
       @method get_last_desc_on_branch
       @return {LevelNode} The last descendant node found.
    */
    ptype.get_last_desc_on_branch = function() {
        if (this.get_child_count() > 0) {
            var child = this.get_lowest_child();
            if (child.branch != this.branch) {
                return this;
            } else {
                return child.get_last_desc_on_branch();
            }
        }
        return this;
    };
    
    /**
       Checks if the node has a descendant on a lower branch than the node itself.
       
       @method has_lower_descendant
       @return {boolean} True if a lower descendant exists, otherwise false.
    */
    ptype.has_lower_descendant = function() {
        if (this.branch != 0) { 
            var desc = this.get_last_desc_on_branch()
            if (desc.get_child_count() > 0 && 
                desc.get_lowest_child().branch < this.branch) {
                return true;
            }
        }
        return false;
    };
    
    platformer.NodeCoords = NodeCoords;
    platformer.LevelNode = LevelNode;
}());
