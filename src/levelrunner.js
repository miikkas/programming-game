/**

*/
/*
TODO:
    - Level creation
        - Creating trees
        - ‎Allowed elements
        - Goal
    - Level run logic
        - Character movement
        - When to execute tile action
        - Executing tile properties
        - Teleporting and changes in elements
        
    - Building logic 
        - Placement of tiles
        - Interfacing with level to get allowed operations
        
        
    
    elements                exec:                       param:
        "pass"              (player) 
        "for_jump"          (player, param, colors)         for_stmt    element
        "for_stmt"          (player, param, colors)         iterator    array
        "push"              (player, param, colors)         item        str
        "pop"               (player, param, colors)         item        str
        "delete_stmt"       (player, param, colors)         
        "assign"            (player, param, colors)         iterator    array
        "push_random"       (player, param, colors)         items
        "func_call"         (player, param, colors)         tree
        "return_stmt"       (player, param, colors)         
        "conditional"       (player, param, colors)         conditions
        "sys_exit"          (player, param, colors)         iterator
        
    How to create an element:
        new platformer.Element(type, param, colors);
        
    How to create a node:
        new platformer.LevelNode(id, element, parents, children);
        
    Level:
        new platformer.Level(trees, tests);
        new platformer.Level(trees, tests);
        new platformer.BuildLevel(trees, tests, opt_elements, req_elements, maxdepth, maxlift);
        
       
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {
    
    var p = platformer;
    
    /**
       A LevelRunner object that is responsible for running all the tests for the level.
    
       @class LevelRunner
       @constructor
       @param [tests=[]] {Array} An array of input tests which should be performed for 
       the level.
    */
    var LevelRunner = function(tests, level) {
        this.level = level;
        this.tests = tests.clone();
        this.test_running = -1;
        this.test_input = 0;
        this.test_stack = [];
        this.success_tests = [];
        this.failure_tests = [];
        this.test_gui = null;
        var text = new createjs.Text("", "bold 20px Helvetica", "#222222");
        text.x = 10;
        text.y = 5;
        this.status_text = text;
    };
    
    var ptype = LevelRunner.prototype;
    
    /**
        Starts a level test.
           
        @method start_test
    */
    ptype.start_test = function(gui) {
        gui = p.default_to(gui, this.test_gui);
        this.test_gui = gui;
        gui.init_test();
        if (this.test_stack.length <= 0) {
            this.push_tests();
        }
        var test_number = this.pop_test();
        var tests_max = this.tests.length;
        var completed = this.success_tests.length;
        var failed = this.failure_tests.length;
        this.status_text.text = "Test " + (test_number + 1) + "/" + tests_max + 
                                ", Completed: " + completed + ", Failed: " + failed;
        this.status_text.color = "#222222";
        this.test_running = test_number;
        this.test_input = 0;
    };

    /**
       Push all tests into stack for running.

       @method push_tests
     */
    ptype.push_tests = function() {
        this.test_stack = this.tests.clone();
        this.success_tests = [];
        this.failure_tests = [];
    };

    /**
       Pop the last test from the test stack.
       
       @method pop_test
    */
    ptype.pop_test = function() {
        if (this.test_stack.length > 0) {
            this.test_stack.pop();
            return this.tests.length - this.test_stack.length - 1;
        } else {
            console.log("No tests in stack.");
            return false;
        }
    };
    
    ptype.finish_test = function(test_success) {
        if (test_success == true) {
            this.success_tests.push(this.test_running);
        } else {
            this.failure_tests.push(this.test_running);
        }
        
        if (this.test_stack.length === 0) {
            var failed = this.failure_tests.length;
            if (failed === 0) {
                // All tests have been successfully run!
                this.status_text.text = "Congratulations! Level completed!";
                this.status_text.color = "#22ff22";
            } else {
                this.status_text.text = failed + "/" + this.tests.length + 
                                        " tests failed!";
                this.status_text.color = "#ff2222";
            }
            this.test_running = -1;
            this.test_gui = null;
        } else {
            this.start_test();
        }
    };
    
    ptype.is_test_running = function() {
        if (this.test_running >= 0) {
            return true;
        }
        return false;
    };
    
    ptype.get_next_test_input = function() {
        if (this.is_test_running()) {
            var test = this.tests[this.test_running];
            if (this.test_input >= test.length) {
                this.test_input = 0;
            }
            return test[this.test_input++];
        }
        return null;
    };
    
    ptype.is_test_ready = function() {
        if (this.is_test_running() || this.level.elements.length > 0) {
            console.log(this.is_test_running() + " test running");
            console.log(this.level.elements);
            return false;
        }
        
        var nodes, node;
        var trees = this.level.trees;
        for (i = 0; i < trees.length; i++) {
            nodes = trees[i].get_nodes();
            for (var j = 0; j < nodes.length; j++) {
                node = nodes[j];
                if (node.get_child_count() === 0 && 
                    ![p.SYS_EXIT, p.RETURN_STMT].contains(node.element.type)) {
                    return false;
                }
            }
        }
        return true;
    };
    
    /**
       Move the player to the next tile.

       @method player_move
       @param player
       @param gui
    */
    ptype.player_move = function(player, gui) {
        var tree = gui.level.get_tree_by_name(player.position.tree);
        
        // Get next coordinates.
        var next_node = tree.get_node_by_id(player.position.id);
        
        var coords = gui.get_node_sprite_coords(next_node, player.position.tree);
        var nextX = coords.x;
        var nextY = coords.y - 32;
        var player_gfx = gui.player_gfx;
        
        if (player_gfx.x == nextX && nextY == player_gfx.y) {
            // When dude is exactly on one tile
            // Currently this is measured by comparing next‎X and x
            // nextX should be the next possible valid location where
            // character travels via intermediate pixels (x)
            this.move_to_next(nextX, nextY, player, gui);
        } else {
            // Dude is not on the next tile yet -> get intermediate coordinates
            if (player_gfx.y > nextY) {
                if (player_gfx.y != nextY) {
                    player_gfx.y += (nextY - player_gfx.y)/Math.abs(player_gfx.y - nextY);
                } else if (player_gfx.x != nextX) {
                    player_gfx.x += (nextX - player_gfx.x)/Math.abs(player_gfx.x - nextX);
                }
            } else {
                if (player_gfx.x != nextX) {
                    player_gfx.x += (nextX - player_gfx.x)/Math.abs(player_gfx.x - nextX);
                } else if (gui.player_gfx.y != nextY) {
                    player_gfx.y += (nextY - player_gfx.y)/Math.abs(player_gfx.y - nextY);
                }
            }
        }
    }

    /**
       Update player's location and on few special cases perform
       teleportation etc.

       @method move_to_next
       @param nextX
       @param nextY
       @param player
       @param gui
    */
    ptype.move_to_next = function(nextX, nextY, player, gui) {
        //console.log(gui.player.inventory);
        
        var start = player.position["id"]; 
        var tree = this.level.get_tree_by_name(player.position.tree);
        var start_type = tree.get_node_by_id(start).element.type;
        
        // Updates players positon
        player.interact(this);
        
        var end = player.position["id"];
        var end_type = this.level.get_tree_by_name(player.position.tree).get_node_by_id(end).element.type;
        
        //console.log(player.position.tree + " " + start_type + " " + end_type);
        
        var tree = player.position.level.get_tree_by_name(player.position.tree);
        var next_node = tree.get_node_by_id(player.position.id);
        var coords = gui.get_node_sprite_coords(next_node, player.position.tree);
        var nextX = coords.x;
        var nextY = coords.y - 32;
        var player_gfx = gui.player_gfx;
        
        //console.log(tree.get_node_by_id(start).element.type + " " + tree.get_node_by_id(end).element.type);
        // Shit hits the fan if previous and current elements are of certain types
        if (start_type == "for_jump" && end_type == "for_stmt") { 

            player_gfx.x = nextX;
            player_gfx.y = nextY;
                        
        } else if (start_type == "for_stmt" && end_type == "for_jump") {
            
            player_gfx.x = nextX;
            player_gfx.y = nextY;
            
        } else if (start_type == "sys_exit") {
            
            
        } else if (start_type == "return_stmt") {
            //console.log("return_stmt should now teleport");
            //var next_node = level.get_tree_by_name(player.position.tree).get_node_by_id(player.position.id);
            player_gfx.x = nextX;
            
            player_gfx.y = nextY;
        } else if (start_type == "func_call") {
            //var next_node = level.get_tree_by_name(player.position.tree).get_node_by_id(player.position.id);
            player_gfx.x = nextX;
            player_gfx.y = nextY;
        }

        types_for_this_if = ["for_stmt", "for_jump", "push", "pop",
                             "push_random", "delete_stmt", "assign",
                             "func_call", "return_stmt"]
        if (types_for_this_if.indexOf(start_type) != -1) {
            //console.log(platformer.COLORS);
            //console.log(gui.UI.INVENTORY_X);
            
            //console.log(gui.root_positions[player.position.tree]);
            
            // Remove things from inventory slots
            if (gui.slot_sprites[player.position.tree] != undefined) {
                for (var i = 0; i < gui.slot_sprites[player.position.tree].length; i++) {
                    gui.remove_level_sprite(gui.slot_sprites[player.position.tree][i]);
                }
            }

            // Add the correct things to inventory slots
            var sprites = [];
            var new_sprite = null;
            for (var j = 0; j < 4; j++) {
                for (var i = 0; i < gui.player.inventory[p.COLORS[j]].length; i++) {
                    var new_fruit = gui.player.inventory[p.COLORS[j]][i];
                    var fruit_x = 19 + i * 22;
                    var fruit_y = gui.root_positions[player.position.tree] - (32 + 5) * j;
                    new_sprite = gui.add_level_sprite(new_fruit, fruit_x, fruit_y);
                    sprites.push(new_sprite);
                }
            }
            
            gui.slot_sprites[player.position.tree] = sprites;
        } 
        
        if (start_type == "for_stmt") {
            // Remove things from for slots
            if (gui.for_slot_sprites[start] != undefined) {
                for (var i = 0; i < gui.for_slot_sprites[start].length; i++) {
                    gui.remove_level_sprite(gui.for_slot_sprites[start][i]);
                }
            }
            
            var for_sprites = [];
            var new_sprite = null;

            // Add the correct things to for slots
            for (var i = 0; i < tree.get_node_by_id(start).element.param.iterator.length; i++) {
                var coords = gui.get_node_sprite_coords(tree.get_node_by_id(start), player.position.tree);
                var new_fruit = tree.get_node_by_id(start).element.param.iterator[i];
                var fruit_x = coords.x;
                var fruit_y = coords.y - (32 * 2) - (22) * i;
                new_sprite = gui.add_level_sprite(new_fruit, fruit_x, fruit_y);
                for_sprites.push(new_sprite);
            }
            
            gui.for_slot_sprites[start] = for_sprites;
            
        /*else if (start_type == "conditional" && (tree.get_node_by_id(start).branch != tree.get_node_by_id(end).branch)) {
            console.log("elevator");
            var elevatorTile = tileContainer.getChildByName(start); //Get the tile with elevator graphics
            
        } else if (tree.get_node_by_id(start).element.type == "assign_inc") {
            //draw_items(inventory_grid);
        } else if (tree.get_node_by_id(start).element.type == "for_stmt") {
            //draw_items(inventory_grid);
        } else if (tree.get_node_by_id(start).element.type == "random_val") {
            //draw_items(inventory_grid);
        } */
        
        }
    }

    platformer.LevelRunner = LevelRunner;
}());
