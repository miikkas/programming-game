/**
   A module that defines a Level object, which describes one game level. The Level object
   is inherited by two types of objects: ReadLevel for levels that contain a reading problem
   and BuildLevel for levels that contain a building problem. Also includes a Methods object
   that is used by Level objects.
   
   @module level
*/

/**
   @namespace platformer
*/
this.platformer = this.platformer || {};

(function() {
    
    var p = platformer;
    
    /**
       A Methods object that describes a method pair of Level object. The pair consists of
       a method that checks if an action is allowed and a method that executes that action.
       
       @class Methods
       @constructor
       @param allows {Function} The method that checks if an action is allowed.
       @param action {Function} The method that executes an action.
       @return {Methods} The initialized Methods object.
    */
    var Methods = function(allows, action) {
        this.allows = allows;
        this.action = action;
        return this;
    };
    
    /**
       A Level object that describes a single level in the game and acts as a base object
       for all other level objects.
       
       @class Level
       @constructor
       @param [trees=[new NodeTree(MAIN)]] {Array} An array of trees that each describe a 
       function in the level.
       @return {Level} The initialized Level object.
    */
    var Level = function(trees) {
        this.trees = p.default_to(trees.slice(0, 4), [new p.NodeTree(p.MAIN)]);
        return this;
    };
    
    var ptype = Level.prototype;
    
    /**
       Checks if the level has a node tree with a given name.
       
       @method has_tree_with_name
       @param tree_name {string} The name of the node tree to be checked.
       @return {boolean} True if the tree is found, otherwise false.
    */
    ptype.has_tree_with_name = function(tree_name) {
        if (this.get_tree_by_name(tree_name) != null) {
            return true;
        }
        return false;
    };
    
    /**
       Returns a node tree by name if such tree exists in the level.
       
       @method get_tree_by_name
       @param tree_name {string} The name of the node tree to be searched.
       @return {NodeTree} The node tree object or null if the tree does not exist.
    */
    ptype.get_tree_by_name = function(tree_name) {
        var tree;
        for (var i = 0; i < this.trees.length; i++) {
            tree = this.trees[i];
            if (tree.name === tree_name) {
                return tree;
            }
        }
        return null;
    };
    
    ptype.get_node = function(tree_name, node_id) {
        if (!this.has_tree_with_name(tree_name)) {
            return null;
        }
        return this.get_tree_by_name(tree_name).get_node_by_id(node_id);
    }
    
    /**
       Returns a node tree and node by name and node id if such tree and node exist in the
       level.
       
       @method get_tree_and_node
       @param tree_name {string} The name of the node tree to be searched.
       @param node_id {int} The id of the node to be searched.
       @return {Object} Returns an anonymous object that contains the tree and the node (or
       null if the searched object was not found).
    */
    ptype.get_tree_and_node = function(tree_name, node_id) {
        if (!this.has_tree_with_name(tree_name)) {
            return {tree : null, node : null};
        }
        var tree = this.get_tree_by_name(tree_name);
        return {tree : tree, node : tree.get_node_by_id(node_id)};
    };
    
    /**
       Checks if the level contains a given node tree.
       
       @method has_tree
       @param tree {NodeTree} The node tree to be checked.
       @return {boolean} True if the level contains the tree, otherwise false.
    */
    ptype.has_tree = function(tree) {
        if (this.trees.contains(tree)) {
            return true;
        }
        return false;
    };
    
    /**
       Checks if the level contains a given node tree and node.
       
       @method has_tree_with_node
       @param tree {NodeTree} The node tree to be checked.
       @param node {LevelNode} The node to be checked.
       @return {boolean} True if both the tree and node are found, otherwise false.
    */
    ptype.has_tree_with_node = function(tree, node) {
        if (this.has_tree(tree) && 
            node != null &&
            this.get_tree_by_name(tree.name).get_node_by_id(node.id)) {
            return true;
        }
        return false;
    };

    /**
       A ReadLevel object that describes a single level in the game, which contains a
       reading problem.
       
       @class ReadLevel
       @constructor
       @extends Level
       @param [trees=[new NodeTree(MAIN)]] {Array} An array of trees that each describe a 
       function in the level.
       @return {ReadLevel} The initialized ReadLevel object.
    */
    var ReadLevel = function(trees) {
        Level.call(this, trees);
        return this;
    };
    
    ReadLevel.prototype = Object.create(ptype);
    r_ptype = ReadLevel.prototype;
    
    r_ptype.get_goal_count = function() {
        var nodes;
        var goal_count = 0;
        for (var i = 0; i < this.trees.length; i++) {
            nodes = this.trees[i].get_nodes();
            for (var j = 0; j < nodes.length; j++) {
                if (nodes[i].element.type === p.SYS_EXIT) {
                    goal_count++;
                }
            }
        }
        return goal_count;
    };
    
    r_ptype.set_finish_condition = function(tree_name, node_id, condition) {
        var node = this.get_node(tree_name, node_id);
        if (node != null && node.element.type === p.SYS_EXIT) {
            node.element.param = condition;
        }
    };
    
    /**
       A BuildLevel object that describes a single level in the game, which contains a
       building problem.
       
       @class BuildLevel
       @constructor
       @extends Level
       @param [trees=[new NodeTree(MAIN)] {Array} An array of trees that each describe a 
       function in the level.
       @param [elements=[]] {Array} An array of Element objects that can be assigned to
       the nodes of the node trees of the level.
       @param [maxdepth=16] {int} The maximum allowed depth of the trees in the level.
       @param [maxlift=3] {int} The maximum allowed number of lifting the conditional 
       elements in the level.
       @return {BuildLevel} The initialized BuildLevel object.
    */
    var BuildLevel = function(trees, elements, maxdepth, maxlift) {
        Level.call(this, trees);
        
        this.elements = elements;
        this.maxdepth = p.default_to(maxdepth, 16);
        this.maxlift = p.default_to(maxlift, 3);
        this.action_mapping = {add : new Methods(this.allows_adding, 
                                                 this.add_platform),
                               remove : new Methods(this.allows_removing, 
                                                    this.remove_platform),
                               connect : new Methods(this.allows_connecting, 
                                                     this.connect_platform),
                               disconnect : new Methods(this.allows_disconnecting,
                                                        this.disconnect_platform),
                               clear : new Methods(this.allows_clearing,
                                                   this.clear_element),
                               lift : new Methods(this.allows_lifting,
                                                  this.lift_branches)};
        this.method = function() {};
        return this;                                
    };
    
    BuildLevel.prototype = Object.create(ptype);
    b_ptype = BuildLevel.prototype;
    
    /**
       Returns the actions that are allowed for the platform that is represented by the node
       with a given id in the node tree with a given name.
       
       @method get_allowed_actions
       @param tree_name {string} The name of the tree in which the node resides.
       @param node_id {int} The id of the node which represents the platform.
       @return {Array} An array of allowed actions as strings.
    */
    b_ptype.get_allowed_actions = function(tree_name, node_id) {
        var methods;
        var actions = [];
        var coords = this.get_tree_and_node(tree_name, node_id);
        
        for (action in this.action_mapping) {
            this.method = this.action_mapping[action].allows;
            if (this.method(coords.tree, coords.node)) {
                actions.push(action);
            }
        }
        return actions;
    };
    
    /**
       Executes a specified action for a platform that is represented by the node with a 
       given id in the node tree with a given name.
       
       @method execute_action
       @param tree_name {string} The name of the tree in which the node resides.
       @param node_id {int} The id of the node which represents the platform.
       @param action {string} A string describing the action.
       @return {boolean} True if the action was performed succesfully, otherwise false.
    */
    b_ptype.execute_action = function(tree_name, node_id, action) {
        if (this.action_mapping.hasOwnProperty(action)) {
            var coords = this.get_tree_and_node(tree_name, node_id);
            this.method = this.action_mapping[action].action;
            if (this.method(coords.tree, coords.node)) {
                return true;
            }
        }
        return false;
    };
    
    b_ptype.get_disabled_elements = function(tree_name, node_id) {
        var element;
        var elements = [];
        var coords = this.get_tree_and_node(tree_name, node_id);
        
        for (var i = 0; i < this.elements.length; i++) {
            element = this.elements[i];
            if (!this.allows_element(coords.tree, coords.node, element)) {
                elements.push(element);
            }
        }
        
        return elements;
    };
    
    /**
       Checks if the level allows adding of a platform node to a given node tree with a 
       given node as a parent.
       
       @method allows_adding
       @param tree {NodeTree} The node tree to be checked.
       @param node {LevelNode} The node to be checked.
       @return {boolean} True if the adding is allowed, otherwise false.
    */
    b_ptype.allows_adding = function(tree, node) {
        if (!this.has_tree_with_node(tree, node)) {
            return false;
        }
        
        if (node.get_child_count() > 0) {
            if (!tree.allows_inner(node, node.get_lowest_child()) ||
                tree.get_depth() >= this.maxdepth) {
                return false;
            }
        } else if (node.depth >= this.maxdepth || !tree.allows_leaf([node])) {
            return false;
        }
        return true;
    };
    
    /**
       Checks if the level allows removing of a given platform node from a given node tree.
       
       @method allows_removing
       @param tree {NodeTree} The node tree to be checked.
       @param node {LevelNode} The platform node to be checked.
       @return {boolean} True if the removing is allowed, otherwise false.
    */
    b_ptype.allows_removing = function(tree, node) {
        if (!this.has_tree_with_node(tree, node) ||
            !tree.allows_removing(node)) {
            return false;
        }
        return true;
    };
    
    /**
       Checks if the level allows connecting of a given platform node in a given node tree
       to a platform below.
       
       @method allows_connecting
       @param tree {NodeTree} The node tree to be checked.
       @param node {LevelNode} The platform node to be checked.
       @return {boolean} True if the connecting is allowed, otherwise false.
    */
    b_ptype.allows_connecting = function(tree, parent) {
        if (!this.has_tree_with_node(tree, parent)) {
            return false;
        }
        var branch = parent.branch_node.branch;
        var depth = parent.depth + 1;
        
        if (tree.has_node_with_coords(depth, branch)) {
            var child = tree.get_node_by_coords(depth, branch);
            if (tree.allows_connecting(parent, child) && child.element.type === p.PASS) {
                var n, stmt_node;
                var nodes = tree.get_nodes();
                for (var i = 0; i < nodes.length; i++) {
                    n = nodes[i];
                    if (n.element.type === p.FOR_JUMP && n.branch === branch) {
                        stmt_node = tree.get_node_by_element(n.element.param);
                        if (parent.branch_node.depth < stmt_node.depth &&
                            stmt_node.depth < depth && depth < n.depth) {
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        return false;
    };
    
    /**
       Checks if the level allows connecting of a given platform node in a given node tree
       to a platform below.
       
       @method allows_connecting
       @param tree {NodeTree} The node tree to be checked.
       @param node {LevelNode} The platform node to be checked.
       @return {boolean} True if the connecting is allowed, otherwise false.
    */
    b_ptype.allows_disconnecting = function(tree, parent) {
        if (!this.has_tree_with_node(tree, parent)) {
            return false;
        }
        
        if (tree.allows_disconnecting(parent, parent.get_lowest_child())) {
            return true;
        }
        return false;
    };
    
    b_ptype.allows_for_jump = function(tree, node, element) {
        var stmt_node = tree.get_node_by_element(element.param);
        
        if (!this.has_tree_with_node(tree, node) ||
            stmt_node === null ||
            stmt_node.depth >= node.depth ||
            stmt_node.branch != node.branch ||
            stmt_node.branch_node != node.branch_node) {
            return false;
        }
        
        var n;
        var stmt_count = 0;
        var jump_count = 0;
        for (var d = stmt_node.depth + 1; d < node.depth; d++) {
            n = tree.get_node_by_coords(d, node.branch);
            if (n.get_parent_count() > 1 && 
                n.get_highest_parent().branch_node.depth < stmt_node.depth) {
                return false;
            } else if (n.element.type === p.FOR_STMT) {
                stmt_count++;
            } else if (n.element.type === p.FOR_JUMP) {
               jump_count++;
            }
            if (stmt_count != jump_count) {
                return false;
            }
        }
        return true;
    };
    
    /**
       Checks if the level allows a specified element to a given platform node in a given
       node tree.
       
       @method allows_element
       @param tree {NodeTree} The node tree to be checked.
       @param node {LevelNode} The platform node to be checked.
       @param element {Element} The element to be checked.
       @return {boolean} True if the element is allowed, otherwise false.
    */
    b_ptype.allows_element = function(tree, node, element) {
        var type = element.type;
        
        if (!this.has_tree_with_node(tree, node) || 
            node.get_parent_count() > 1 ||
            node === tree.root ||
            (type === p.FOR_JUMP && !this.allows_for_jump(tree, node, element))) {
            return false;
        } else if (type === p.CONDITIONAL) {
            if ((node.get_child_count() === 0 || 
                node.get_lowest_child().branch < node.branch) && 
                !this.allows_adding(tree, node)) {
                return false;
            } else if (!tree.allows_leaf([node])) {
                return false;
            }
        }
        return true;
    };
    
    /**
       Checks if the level allows clearing an element of a given platform node in a given 
       node tree.
       
       @method allows_clearing
       @param tree {NodeTree} The node tree to be checked.
       @param node {LevelNode} The platform node to be checked.
       @return {boolean} True if the clearing is allowed, otherwise false.
    */
    b_ptype.allows_clearing = function(tree, node) {
        if (!this.has_tree_with_node(tree, node) ||
            node.element.type === p.PASS) {
            return false;
        }
        return true;
    };
    
    /**
       Checks if the level allows lifting of upper child branches of a given platform node in 
       a given node tree.
       
       @method allows_lifting
       @param tree {NodeTree} The node tree to be checked.
       @param node {LevelNode} The platform node whose child branches are to be checked.
       @return {boolean} True if the lifting is allowed, otherwise false.
    */
    b_ptype.allows_lifting = function(tree, node) {
        if (!this.has_tree_with_node(tree, node) || node.get_child_count() < 2) {
            return false;
        }
        var children = node.children.omit(node.get_lowest_child());
        var child = p.LevelNode.find_lowest_node(children);
        if (child.branch - node.branch >= this.maxlift) {
            return false;
        }
        return true;
    };
    
    /**
       Adds new platform(s) to a specified node tree after a given platform node if 
       possible.
       
       @method add_platform
       @param tree {NodeTree} The node tree to which the new platform nodes are added.
       @param node {LevelNode} The node after which the platform nodes are added.
       @return {boolean} True if the adding was succesful, otherwise false.
    */
    b_ptype.add_platform = function(tree, node) {
        if (!this.allows_adding(tree, node)) {
            return false;
        }
        
        if (node.get_child_count() === 0) {
            tree.add_leaf(new p.LevelNode(), [node]);
        } else {
            tree.add_inner(new p.LevelNode(), node, node.get_lowest_child());
        }
        
        return true;
    };
    
    /**
       Removes a given platform node from a specified node tree and the necessary other
       nodes with it if possible.
       
       @method remove_platform
       @param tree {NodeTree} The node tree from which the platform node(s) are removed.
       @param node {LevelNode} The platform node to be removed.
       @return {boolean} True if the removing was succesful, otherwise false.
    */
    b_ptype.remove_platform = function(tree, node) {
        if (!this.allows_removing(tree, node)) {
            return false;
        }
        
        var n;
        var nodes_before = tree.get_nodes();
        tree.remove_node(node);
        var nodes_after = tree.get_nodes();
        
        for (var i = 0; i < nodes_before.length; i++) {
            n = nodes_before[i];
            if (!nodes_after.contains(n)) {
                if (n.element.type != p.PASS) {
                    this.elements.push(n.element);
                }
            } else if (n.element.type === p.CONDITIONAL && n.get_child_count() < 2) {
                this.elements.push(n.element);
                n.element = new p.Element(p.PASS);
            }    
        }
        if (node.element.type === p.FOR_STMT) {
            this.clear_for_jump_pair(node, nodes_after);
        }
        
        return true;
    };
    
    /**
       Connects a given platform node from a specified node tree to a node below if 
       possible.
       
       @method connect_platform
       @param tree {NodeTree} The node tree that contains the platform node.
       @param node {LevelNode} The platform node to be connected.
       @return {boolean} True if the connecting was succesful, otherwise false.
    */
    b_ptype.connect_platform = function(tree, node) {
        if (!this.allows_connecting(tree, node)) {
            return false;
        }

        tree.connect_nodes(node, tree.get_node_by_coords(node.depth + 1, 
                                                         node.branch_node.branch));
        return true;
    };
    
    /**
       Disconnects a given platform node in a specified node tree from a node below if
       possible.
       
       @method disconnect_platform
       @param tree {NodeTree} The node tree that contains the platform node.
       @param node {LevelNode} The platform node to be disconnected.
       @return {boolean} True if the disconnecting was succesful, otherwise false.
    */
    b_ptype.disconnect_platform = function(tree, node) {
        if (!this.allows_disconnecting(tree, node)) {
            return false;
        }

        tree.disconnect_nodes(node, node.get_lowest_child());
        return true;
    };
    
    /**
       Clears a conditional element of a given node residing in a given tree from the 
       level, if possible, and adds it back to the element array of the level. Also the upper 
       branches of that node are removed.
       
       @method clear_conditional
       @param tree {NodeTree} The node tree that contains the node.
       @param node {LevelNode} The platform node, which contains the conditional element.
       @return {boolean} True if the clearing was succesful, otherwise false.
    */
    b_ptype.clear_conditional = function(tree, node) {
        if (node.element.type != p.CONDITIONAL || 
            !this.has_tree_with_node(tree, node)) {
            return false;
        }
        
        var n;
        var nodes_before = tree.get_nodes();
        tree.remove_higher_child_branches(node);
        tree.move_ancestor_branches(node, node.branch + 1)
        var nodes_after = tree.get_nodes();
        
        for (var i = 0; i < nodes_before.length; i++) {
            n = nodes_before[i];
            if (!nodes_after.contains(n)) {
                if (n.element.type != p.PASS) {
                    this.elements.push(n.element);
                }
            } else if (n.element.type === p.CONDITIONAL && n.get_child_count() < 2) {
                this.elements.push(n.element);
                n.element = new p.Element(p.PASS);
            }    
        }
        
        return true;
    };
    
    b_ptype.clear_for_jump_pair = function(node, nodes) {
        var n;
        
        for (var i = 0; i < nodes.length; i++) {
            n = nodes[i];
            if (n.element.type === p.FOR_JUMP && 
                n.element.param === node.element) {
                this.elements.push(n.element);
                n.element = new p.Element(p.PASS);
                break;
            }
        }
    };
    
    /**
       Clears a for_stmt element of a given node (and its corresponding for_jump pair) 
       residing in a given tree from the level, if possible, and adds it back to the 
       element array of the level.
       
       @method clear_for_stmt
       @param tree {NodeTree} The node tree that contains the node.
       @param node {LevelNode} The platform node, which contains the for_stmt element.
       @return {boolean} True if the clearing was succesful, otherwise false.
    */
    b_ptype.clear_for_stmt = function(tree, node) {
        if (node.element.type != p.FOR_STMT || !this.has_tree_with_node(tree, node)) {
            return false;
        }
        var nodes = tree.get_nodes();
        
        this.elements.push(node.element);
        this.clear_for_jump_pair(node, nodes);
        node.element = new p.Element(p.PASS);
        return true;
    };
    
    /**
       Clears an element of a given platform node residing in a given tree from the level, 
       if possible, and adds it back to the element array of the level. Performs also the
       other necessary operations that are associated with the clearing, such as removing 
       the pair of the element or removing branches originating from the node that contains.
       the element.
       
       @method clear_for_stmt
       @param tree {NodeTree} The node tree that contains the node.
       @param node {LevelNode} The platform node, which contains the element.
       @return {boolean} True if the clearing was succesful, otherwise false.
    */
    b_ptype.clear_element = function(tree, node) {
        if (this.clear_conditional(tree, node) || 
            this.clear_for_stmt(tree, node)) {
            return true;
        } else if (this.has_tree_with_node(tree, node) &&
                   this.allows_clearing(tree, node)) {
            this.elements.push(node.element);
            node.element = new p.Element(p.PASS);
            return true;
        }
        return false;
    };

    /**
       Places a given element to the platform node with a given id, residing in the node 
       tree with a given name, if possible. If the node had a previous element, it is 
       added to the element array of the level. All the operations associated to the
       changing are also performed, such as adding of child nodes.
       
       @method place_element
       @param tree_name {string} The name of the tree that contains the node.
       @param node_id {int} The id of the node, whose element is to be changed.
       @return {boolean} True if the changing was succesful, otherwise false.
    */
    b_ptype.place_element = function(tree_name, node_id, element) {
        var coords = this.get_tree_and_node(tree_name, node_id);
        var node = coords.node;
        var tree = coords.tree;
        
        if (!this.allows_element(tree, node, element) || 
            !this.elements.contains(element)) {
            return false;
        }
        
        if (this.allows_clearing(tree, node)) {
            this.clear_element(tree, node);
        }
        this.elements.remove(element);
        node.element = element;
        
        if (element.type === p.CONDITIONAL) {
            var conditions = element.param;
            if (node.get_child_count() === 0) {
                tree.add_leaf(new p.LevelNode(), [node]);
            } else if (node.get_lowest_child().branch < node.branch) {
                tree.add_inner(new p.LevelNode(), node, node.get_lowest_child());
            }
            for (var i = 0; i < conditions.length; i++) {
                tree.add_leaf(new p.LevelNode(), [node]);
            }
        }
        return true;
    };
    
    /**
       Lifts the upper child branches of a given platform node, residing in a given node
       tree, if possible.
       
       @method lift_branches
       @param tree {NodeTree} The node tree that contains the node.
       @param node {LevelNode} The platform node, whose child branches are to be lifted.
       @return {boolean} True if the lifting was succesful, otherwise false.
    */
    b_ptype.lift_branches = function(tree, node) {
        if (!this.allows_lifting(tree, node)) {
            return false;
        }
        
        var child;
        for (var i = 0; i < node.get_child_count(); i++) {
            child = node.children[i];
            if (child.branch > node.branch) {
                tree.move_child_branches(child, child.branch + 1);
            }
        }
        tree.move_ancestor_branches(node, node.get_highest_child().branch + 1);
        return true;
    };
    
    platformer.Level = Level;
    platformer.ReadLevel = ReadLevel;
    platformer.BuildLevel = BuildLevel;
}());
