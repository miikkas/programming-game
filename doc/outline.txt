Level
=====

- Each "mission" in the game has one map.
- Each map consists of multiple levels.
- Each level is a tree, whose leaf nodes can connect to a mutual child node.
    * The tree represents a function.
    * Each tree has a name; each map has at least one level (tree) whose name is "main".
    * In-game function calls moves the player to the beginning of a function (level (tree)) of that name.
    * When an in-game function is removed from the call stack, i.e. the function returns, the player is
      moved back to where they came from.
        -> A call stack must be implemented!
            + With saved player coordinate and inventory data at each level

Level node
==========

- Each node in a tree is associated with a level component type and it's parameters.

Level component type
====================

- A level component type selects the functionality (is it an if, for etc.)
    * Must be scripted somehow; the script is contained in the component type.
- A level component type has its associated graphics.

Player
======

- The player has an in-game function call stack, i.e. coordinate & inventory at each level
- The player has an inventory (a list of items)
- The player has a coordinate, a point where they are at a level tree
- The player has its associated graphics.

Item
====

- Items can be collected and stored in the player's inventory.
- Item has an id, a type and associated graphics.
- Item instances have a quantity.
