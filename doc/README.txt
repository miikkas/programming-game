Instructions
============

To play the game or access the unit tests the 'product' directory must be
served with an HTTP server. Python 2 and 3 both provide a simple HTTP server
which should suffice.

0. Uncompress this archive and enter its directory in a terminal session.

1. Enter the 'product' directory.

    $ cd product

2.a. Python 2 - start the HTTP server.

    $ python2 -m SimpleHTTPServer

2.b. Python 3 - start the HTTP server.

    $ python3 -m http.server

3. Open the address http://localhost:8000 with your Web browser (Firefox or
   Chrome recommended and tested).

4.a. View the unit tests by browsing the directory structure or, alternatively,
   by going straight to the following URL:
   
    http://localhost:8000/tests/run_tests.html

4.b. Play the game by browsing the directory structure or, alternatively,
   by going straight to the following URL:

    http://localhost:8000/src/rungame.html


