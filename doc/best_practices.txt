- Use 4 space indentations in code, not tabs.
- Make atomic commits, i.e. commit each separate task separately.
- Use UTF-8 _WITHOUT_ Byte-Order-Mark in all text/code files.
- Use lowercase with underscores when naming variables.

