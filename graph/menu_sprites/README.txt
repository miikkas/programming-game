Creating a sprite sheet
-----------------------

To create the sprite sheet from the individual images:

    $ rm sprites.png
    $ convert *.png +append 'sprites.png'

This deletes the old sprites.png to avoid appending it to the new one. After
that, a horizontally combined version of the single images is created.

Alternatively, run the supplied script that does the above automatically:

    $ ./create_spritesheet.sh

Requires ImageMagick or GraphicsMagick.


Getting EaselJS spritesheet data
--------------------------------

In order to get the relevant information for the created spritesheet, run the
following script:

    $ ./getinfo.sh | ./generate_spritesheet_data.py --strip

By omitting the --strip switch you can see the data in human readable form.

