# Bitbucket tips

First time:

$ git clone thehttpsaddressfrombitbucket

Basic workflow:

- Get the changes that others have made:

$ git pull

- Create new files, modify existing files
- Show the list of changed files:

$ git status

- Add the new/changed files to a commit:

$ git add file_that_was_changed.js
$ git add my_new_beautiful_file.html

- Commit the changes to a local tree

$ git commit -m "Fixed a typo and added a test HTML file."

- GOTO 1
- When done editing, ready to share the changes to others:

$ git push -u origin master
